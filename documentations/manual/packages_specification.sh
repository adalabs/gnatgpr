#!/bin/bash

ROOT_PATH=source

rm -rf $ROOT_PATH/packages_specification.rst
rm -rf $ROOT_PATH/gnat_gpr*.rst



# 
echo "====================================
Packages specification
====================================

.. toctree::
   :maxdepth: 2

" >> $ROOT_PATH/packages_specification.rst



for i in `ls -1 ../../sources/gnat_gpr*.ads`; do NAME=`basename $i | sed -e "s/\(.*\).ads/\1/"`; PACKAGE_NAME=`echo $NAME | sed -e "s/-/./g"`;echo "   $NAME.rst" >> $ROOT_PATH/packages_specification.rst; echo "========================================================================
$PACKAGE_NAME
========================================================================

.. literalinclude:: ../../../sources/$NAME.ads
    :language: ada
    :linenos:


" > $ROOT_PATH/$NAME.rst ;done

