#!/bin/bash

./packages_specification.sh
make latexpdf
make html
rm -rf html/
cp -r build/html/ .
rm -rf pdf/*
cp -r build/latex/gnatgpr.pdf pdf/
