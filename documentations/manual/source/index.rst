.. GNATGPR documentation master file, created by
   sphinx-quickstart on Wed Jan  1 04:33:11 2003.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GNATGPR's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 3
   :numbered:

   license.rst
   user_manual.rst
   developer_manual.rst
   architecture.rst
   packages_specification.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

