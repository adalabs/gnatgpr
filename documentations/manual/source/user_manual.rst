==================
User Manual
==================

Introduction
################

GNATGPR allows the user to proceed information request on GNAT GPR project files, like for example :
 * Give all included projects (transitive or not, excluding externally built projects or not)
 * Give all included sources (transitive or not, excluding externally built projects or not)
 * Give all included source directories (transitive or not, excluding externally built projects or not)
 * Give all main files (transitive or not)
 * Give all executable files (transitive or not)
 * Give all sources that have visibility on a given unit (transitive or not, excluding externally built projects or not)
 * Give all main directories
 * Give all included object paths (transitive or not, excluding externally built projects or not)
 * Give project zombies (duplicated .o or .ali when a source file are moved from one project to another) (transitive or not, excluding externally built projects or not)

There are 2 ways of accessing these services :
 * In the Shell using the gnatgpr binary
 * In Ada using the GNAT_GPR package specification interface

GNATGPR is based on :
 * GNATColl
 * `AdaControl <http://pagesperso-orange.fr/adalog/adacontrol1.htm>`_ (GMGPL), for option analysis

   * Only the Options_Analyzer package have been re-used (without any change)

 * AUnit and AUnit.Extras for unit testing
 * Libadalang for semantic analysis

Thanks to all the people behind these projects.

GNATGPR Available compilers & platforms
#############################################

 * gnatgpr uses GNAT-specific packages (given it is based on GNATColl)
 * gnatgpr should work on all GNAT platforms


Installation
################


Building GNATGPR from source
******************************

Prerequisites

The following software must be installed in order to compile GNATGPR from source:

* A GNAT compiler, any recent version
* GNATColl
* Libadalang
* AUnit.Extras

Build with project file
==============================

Simply go to the morpheus directory and type:

.. code-block:: bash

   gprbuild -P gnatgpr-bin.gpr

You're done!

Build with Makefile
========================

If you need more flexibility with the location where various components are installed, it is possible to build GNATGPR with a regular Makefile.

The file Makefile (in directory gnatgpr) includes several variables that can be modified to suit your needs. Please refer to the comments in Makefile for more information. Then, run the make command:

.. code-block:: bash

   $ make build

It is also possible to do other actions with this “Makefile”, like deleting object files:

.. code-block:: bash

   $ make clean


Installing GNATGPR
************************

.. code-block:: bash
   
   $ make install PREFIX=/usr/local


Program usage
################

Command line syntax
**********************

GNATGPR is a command-line program, i.e. it is normally called directly from the system bash. The syntax for invoking gnatgpr is:

.. code-block:: bash
 
       gnatgpr -[e][n]p <project file>
                  Display all included <p>rojects.
                  Exclude <e>xternally built project when e option is given.
       gnatgpr -[e][n]s <project file>
                  Display all included <s>ources.
                  <N>on-transitive analysis if n option is given.
                  Exclude <e>xternally built project when e option is given.
       gnatgpr -[e][n] <project file> -w <unit> [-a <unit>] [t] [-f output] [-x <base path> -y <relocate path>]
                  Display sources that have visibility on <unit>
                  <N>on-transitive analysis if n option is given.
                  Exclude <e>xternally built project when e option is given.
                  Display ASIS <t>ree files if t option is given.
                  Use <a>dditional unit if a option is given.
                  Store results in output if f option is given.
                  Base path (that should be a part of project file absolute name) will be replaced by relocate path.
       gnatgpr -[e][n]sd <project file>
                  Display all included <s>ource <d>irectories.
                  <N>on-transitive analysis if n option is given.
                  Exclude <e>xternally built project when e option is given.
       gnatgpr -[e][n]sp <project file>
                  Display all included <s>ources and their corresponding <p>rojects
                  <N>on-transitive analysis if n option is given.
                  Exclude <e>xternally built project when e option is given.
       gnatgpr -[n]b <project file>
                  Display all project <b>inaries absolute names.
                  <N>on-transitive analysis if n option is given.
       gnatgpr -bd <project file>
                  Display project <b>inary <d>irectory.
       gnatgpr -[n]m <project file>
                  Display all <m>ain files.
                  <N>on-transitive analysis if n option is given.
       gnatgpr -[e][n]o <project file>
                  Display all included <o>bject paths
                  <N>on-transitive analysis if n option is given.
                  Exclude <e>xternally built project when e option is given.
       gnatgpr -[n][e][s][l]z <project file>
                  Display all project objects <z>ombies files (.o and .ali) or project <s>ource zombies files (.ads .adb) if s option is given
                  <N>on-transitive analysis if n option is given.
                  Exclude <e>xternally built project when e option is given.
                  Exclude <l>ibrary based project.
       gnatgpr -h
                  Display <h>elp
       gnatgpr -v
                  Display <v>ersion

Here is a real life example:

.. code-block:: bash
   
   gnatgpr -s project.gpr

If gnatgpr is invoked with the “-h” (help) option, it just prints a brief summary of its syntax and exits. Similarly, if it is invoked with the “-v” (version) option, it prints its version numbers. In both of these cases, any other parameter is ignored.

Return codes
****************

GNATGPR returns with an error code of 0 if successful, and an error code of 1 in case of any error.

