==================
 Developer Manual
==================

General guidelines
###############################


Design principles
**********************

* `Single Responsability Principle <http://en.wikipedia.org/wiki/Single_responsibility_principle>`_
   One package/Class has only one responsibility
* `Demeter rules <http://en.wikipedia.org/wiki/Demeter_principle>`_
* All Packages that should not be accessed by the end-user should be private.

Version naming convention
*******************************

GNATGPR release is of the form A.B.C

A, B, C should be incremented on the following conditions

.. cmdoption:: A

   Major architecture/feature change

.. cmdoption:: B 

   End-user Interface/Formats change

.. cmdoption:: C

   Feature/Fix added

Manually update version file when needed

.. code-block:: ada

   package GNATGPR.Versions is

