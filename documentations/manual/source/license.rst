==================
License
==================

::

   GNATGPR is copyright (C) 2012-2021, AdaLabs Ltd. Contact sales@adalabs.com to get GNATGPR under GMGPL
   License.

   https://adalabs.com

   GNATGPR is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   
   This document may be copied, in whole or in part, in any form or by any means, as is or with alterations,    
   provided that (1) alterations are clearly marked as alterations and (2) this copyright notice is 
   included unmodified in any copy.

