#!/bin/bash
BASE_PATH=$ADALABS_ROOT_PATH/gnatgpr

export ADA_PROJECT_PATH=$BASE_PATH:$ADA_PROJECT_PATH
export LD_LIBRARY_PATH=$BASE_PATH/libraries:$LD_LIBRARY_PATH

export PATH=$BASE_PATH/binaries:$PATH

