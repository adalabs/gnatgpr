# gnatgpr

GNATGPR is the swiss knife for GPR projects.
It allows the user to query a GNAT GPR project file content, from the console or a dedicated Ada API

## Documentation
See <https://gl.githack.com/adalabs/gnatgpr/raw/main/documentations/manual/html/index.html>

## Project management
https://www.pivotaltracker.com/n/projects/1080004

## Authors
David SAUVAGE - AdaLabs Ltd

## Dependencies
- GNATColl
- Libadalang
- AUnit and AUnit.Extras for tests

## License
GPL version 3

