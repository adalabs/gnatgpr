
PROJECT_LIB=gnatgpr.gpr
PROJECT_BIN=gnatgpr-bin.gpr
PREFIX?=$(dir $(shell which gnatls))..
DOC_DIR?=share/doc/gnatgpr
OWNER?=root
GROUP?=root
AUNIT?=aunit-3.x
ENV_STATIC=-XLIBRARY_TYPE=static -XASIS_BUILD=static -XXMLADA_BUILD=static
ENV_SHARED=-XLIBRARY_TYPE=relocatable -XASIS_BUILD=shared -XXMLADA_BUILD=relocatable -XGNATCOLL_CORE_BUILD=relocatable
BUILDER=gprbuild
CLEANER=gprclean
GPRINSTALL=gprinstall

build-static-and-shared:
	$(BUILDER) -j0 -P $(PROJECT_BIN) $(ENV_STATIC)
	$(BUILDER) -j0 -P $(PROJECT_LIB) $(ENV_SHARED)

build-static:clean
	$(BUILDER) -j0 -P $(PROJECT_BIN) $(ENV_STATIC)
	$(BUILDER) -j0 -P $(PROJECT_LIB) $(ENV_STATIC)

build-shared:clean
	$(BUILDER) -j0 -P $(PROJECT_LIB) $(ENV_SHARED)

clean:
	$(CLEANER) -r -P $(PROJECT_BIN) $(ENV_STATIC)
	$(CLEANER) -r -P $(PROJECT_BIN) $(ENV_SHARED)
	$(CLEANER) -r -P tests/harness_gnatgpr.gpr -XAUnit=$(AUNIT) $(ENV_STATIC)
	rm -rf objects/*
	rm -rf tests/objects/*


test:build-static
	$(BUILDER) -j0 -P tests/harness_gnatgpr.gpr -XAUnit=$(AUNIT) $(ENV_STATIC)
	./tests/binaries/harness_gnatgpr

install-static:build-static
	$(GPRINSTALL) -p -P $(PROJECT_LIB) --prefix=$(DESTDIR)$(PREFIX) $(ENV_STATIC) --build-name=default
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 -o $(OWNER) -g $(GROUP)  binaries/gnatgpr $(DESTDIR)$(PREFIX)/bin

install-shared:build-shared
	$(GPRINSTALL) -p -P $(PROJECT_LIB) --prefix=$(DESTDIR)$(PREFIX) $(ENV_SHARED) --build-name=shared

install:install-static install-shared
	mkdir -p $(DESTDIR)/$(PREFIX)/$(DOC_DIR)/pdf/
	mkdir -p $(DESTDIR)/$(PREFIX)/$(DOC_DIR)/html/
	cd documentations/manual/html/ ; tar -c ./  | tar -x -C $(DESTDIR)/$(PREFIX)/$(DOC_DIR)/html/
	install -m 0755 -o $(OWNER) -g $(GROUP) documentations/manual/pdf/gnatgpr.pdf $(DESTDIR)/$(PREFIX)/$(DOC_DIR)/pdf/
