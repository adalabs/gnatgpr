--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2017, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

with GPR;

package GNATColl.Projects.Internals is

   function File_Name (Project : Project_Type'Class) return String;

   function Externally_Built (Project : Project_Type'Class) return Boolean;

   procedure Initialize (Error_Handler : GPR.Error_Handler;
                         Self          : out Project_Environment_Access);

   function Source_Files (Project               : Project_Type;
                          Recursive             : Boolean := False;
                          Include_Project_Files : Boolean := False;
                          Exclude_Externally    : Boolean := False;
                          Exclude_Library_Based : Boolean := False)
                          return File_And_Project_Array_Access;

   function Is_Library_Based (Self : Project_Type) return Boolean;

end GNATColl.Projects.Internals;
