--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2021, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

-- Ada
with Ada.Exceptions;

-- GNAT
with GNAT.OS_Lib;

-- GNATGPR
with Options_Analyzer;

package body GNAT_GPR.Options is

   procedure Option_Error (Message : String);
   procedure Option_Error (Occur   : Ada.Exceptions.Exception_Occurrence);

   -- --------------------------------------------------------------------------

   package Analyzer is new Options_Analyzer (Binary_Options => "tpeohsbdmvnzl",
                                             Valued_Options => "wafxy",
                                             Tail_Separator => "--");

   ------------------
   -- Help_Options --
   ------------------

   procedure Help_Options is
   begin
      User_Message ("Usage: ");
      User_Message ("       gnatgpr -[e][n]p <project file>");
      User_Message ("                  Display all included <p>rojects.");
      User_Message ("                  Exclude <e>xternally built project when e option is given.");
      User_Message ("       gnatgpr -[e][n]s <project file>");
      User_Message ("                  Display all included <s>ources.");
      User_Message ("                  <N>on-transitive analysis if n option is given.");
      User_Message ("                  Exclude <e>xternally built project when e option is given.");
      User_Message ("       gnatgpr -[e][n] <project file> -w <unit> [-a <unit>] [t] [-f output] [-x <base path> -y <relocate path>]");
      User_Message ("                  Display sources that have visibility on <unit>");
      User_Message ("                  <N>on-transitive analysis if n option is given.");
      User_Message ("                  Exclude <e>xternally built project when e option is given.");
      User_Message ("                  Display ASIS <t>ree files if t option is given.");
      User_Message ("                  Use <a>dditional unit if a option is given.");
      User_Message ("                  Store results in output if f option is given.");
      User_Message ("                  Base path (that should be a part of project file absolute name) will be replaced by relocate path.");
      User_Message ("       gnatgpr -[e][n]sd <project file>");
      User_Message ("                  Display all included <s>ource <d>irectories.");
      User_Message ("                  <N>on-transitive analysis if n option is given.");
      User_Message ("                  Exclude <e>xternally built project when e option is given.");
      User_Message ("       gnatgpr -[e][n]sp <project file>");
      User_Message ("                  Display all included <s>ources and their corresponding <p>rojects");
      User_Message ("                  <N>on-transitive analysis if n option is given.");
      User_Message ("                  Exclude <e>xternally built project when e option is given.");
      User_Message ("       gnatgpr -[n]b <project file>");
      User_Message ("                  Display all project <b>inaries absolute names.");
      User_Message ("                  <N>on-transitive analysis if n option is given.");
      User_Message ("       gnatgpr -bd <project file>");
      User_Message ("                  Display project <b>inary <d>irectory.");
      User_Message ("       gnatgpr -[n]m <project file>");
      User_Message ("                  Display all <m>ain files.");
      User_Message ("                  <N>on-transitive analysis if n option is given.");
      User_Message ("       gnatgpr -[e][n]o <project file>");
      User_Message ("                  Display all included <o>bject paths");
      User_Message ("                  <N>on-transitive analysis if n option is given.");
      User_Message ("                  Exclude <e>xternally built project when e option is given.");
      User_Message ("       gnatgpr -[n][e][s][l]z <project file>");
      User_Message ("                  Display all project objects <z>ombies files (.o and .ali) or project <s>ource zombies files (.ads .adb) if s option is given");
      User_Message ("                  <N>on-transitive analysis if n option is given.");
      User_Message ("                  Exclude <e>xternally built project when e option is given.");
      User_Message ("                  Exclude <l>ibrary based project.");
      User_Message ("       gnatgpr -h ");
      User_Message ("                  Display <h>elp");
      User_Message ("       gnatgpr -v ");
      User_Message ("                  Display <v>ersion");
   end Help_Options;

   ---------------------
   -- Analyse_Options --
   ---------------------

   procedure Analyse_Options
   is
      use Analyzer;

      procedure Raise_Error_If_Present (Option : Character) is begin
         if Is_Present (Option) then
            raise Analyzer.Options_Error;
         end if;
      end Raise_Error_If_Present;
   begin
      --
      -- Help
      --
      if Is_Present (Option => 'n') then
         Transitive_Analysis := False;
      end if;
      if Is_Present (Option => 'e') then
         Exclude_Externally := True;
      end if;

      if Is_Present (Option => 'f') then
         Output_File_Name := Ada.Strings.Unbounded.To_Unbounded_String (String'(Analyzer.Value (Option => 'f', Explicit_Required => True)));
         Use_Output_File := True;
      end if;

      if Is_Present (Option => 'l') then
         Exclude_Library_Based := True;
      end if;

      if Is_Present (Option => 'h') then
         Raise_Error_If_Present ('n');
         Raise_Error_If_Present ('d');
         Raise_Error_If_Present ('f');
         Action := Help;
      elsif Is_Present (Option => 'v') then
         Raise_Error_If_Present ('n');
         Raise_Error_If_Present ('d');
         Raise_Error_If_Present ('f');
         Action := Version;
      elsif Is_Present (Option => 'p') and Is_Present (Option => 's')
        and Parameter_Count > 0 then
         Raise_Error_If_Present ('d');
         Action                                   := List_Sources_And_Projects;
         Action_Value (List_Sources_And_Projects) := To_Unbounded_String (Source => Parameter (1));

      elsif Is_Present (Option => 'p') and Parameter_Count > 0 then
         Raise_Error_If_Present ('d');
         Action                       := List_Projects;
         Action_Value (List_Projects) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
      elsif Is_Present (Option => 'b') and not Is_Present (Option => 'd')
        and Parameter_Count > 0 then
         Action                       := List_Binaries;
         Action_Value (List_Binaries) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
      elsif Is_Present (Option => 'b') and Is_Present (Option => 'd')
        and Parameter_Count > 0 then
         Raise_Error_If_Present ('n');
         Action                      := List_Binaries_Directories;
         Action_Value (List_Binaries_Directories) := To_Unbounded_String
           (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
      elsif Is_Present (Option => 's') and not Is_Present (Option => 'd') and not Is_Present (Option => 'z')
        and Parameter_Count > 0 then
         Action                      := List_Sources;
         Action_Value (List_Sources) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
      elsif Is_Present (Option => 's') and Is_Present (Option => 'd')
        and Parameter_Count > 0 then
         Action                      := List_Source_Directories;
         Action_Value (List_Source_Directories) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
      elsif Is_Present (Option => 'm') and Parameter_Count > 0 then
         Raise_Error_If_Present ('d');
         Action                    := List_Mains;
         Action_Value (List_Mains) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
      elsif Is_Present (Option => 'o') and Parameter_Count > 0 then
         Raise_Error_If_Present ('d');
         Action                           := List_Object_Paths;
         Action_Value (List_Object_Paths) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
      elsif Is_Present (Option => 'z') and Parameter_Count > 0 then
         Raise_Error_If_Present ('d');
         if Is_Present (Option => 's') then
            Action                             := List_Source_Zombies;
            Action_Value (List_Source_Zombies) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
         else
            Action                             := List_Object_Zombies;
            Action_Value (List_Object_Zombies) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
         end if;
      elsif Is_Present (Option => 'w') then

         Action                                              := List_Sources_That_Have_Visibility_On;
         Action_Value (List_Sources_That_Have_Visibility_On) := To_Unbounded_String (Source => GNAT.OS_Lib.Normalize_Pathname (Parameter (1)));
         With_Entity := Ada.Strings.Unbounded.To_Unbounded_String (String'(Analyzer.Value (Option => 'w', Explicit_Required => True)));
         if Is_Present (Option => 't') then
            Asis_Tree_Files := True;
         end if;
         if Is_Present (Option => 'a') then
            Additional_Unit := Ada.Strings.Unbounded.To_Unbounded_String (String'(Analyzer.Value (Option => 'a', Explicit_Required => True)));
         end if;
         if Is_Present (Option => 'x') then
            declare
               V : constant String := String'(Analyzer.Value (Option => 'x', Explicit_Required => True));
            begin
               if V (V'Last) /= GNAT.OS_Lib.Directory_Separator then
                  Base_Path := Ada.Strings.Unbounded.To_Unbounded_String (V & GNAT.OS_Lib.Directory_Separator);
               else
                  Base_Path := Ada.Strings.Unbounded.To_Unbounded_String (V);
               end if;
            end;
            if not Is_Present (Option => 'y') then
               Option_Error ((" y option is mandatory with x option"));
            end if;
         end if;
         if Is_Present (Option => 'y') then
            declare
               V : constant String := String'(Analyzer.Value (Option => 'y', Explicit_Required => True));
            begin
               if V (V'Last) /= GNAT.OS_Lib.Directory_Separator then
                  Relocate_Path := Ada.Strings.Unbounded.To_Unbounded_String (V & GNAT.OS_Lib.Directory_Separator);
               else
                  Relocate_Path := Ada.Strings.Unbounded.To_Unbounded_String (V);
               end if;
            end;
            if not Is_Present (Option => 'x') then
               Option_Error ((" x option is mandatory with y option"));
            end if;
         end if;

      else
         Raise_Error_If_Present ('n');
         Raise_Error_If_Present ('d');
         Action := Help;
      end if;

   exception
      when Occur : Analyzer.Options_Error =>
         Help_Options;
         Option_Error (Occur);
   end Analyse_Options;

   ------------------
   -- User_Message --
   ------------------

   procedure User_Message (Message : String)
   is
   begin
      Ada.Text_IO.Put_Line (Ada.Text_IO.Current_Error,
                            Message);
   end User_Message;

   ------------------
   -- Option_Error --
   ------------------

   procedure Option_Error (Message : String)
   is
      use Ada.Exceptions;
   begin
      Raise_Exception (Options_Error'Identity, Message);
   end Option_Error;

   ------------------
   -- Option_Error --
   ------------------

   procedure Option_Error (Occur : Ada.Exceptions.Exception_Occurrence)
   is
      use Ada.Exceptions;
   begin
      Option_Error (Exception_Message (Occur));
   end Option_Error;

end GNAT_GPR.Options;
