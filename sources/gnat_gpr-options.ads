--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2020, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;

package GNAT_GPR.Options is

   Options_Error : exception;

   type T_Action_Kinds is (Help,
                           List_Sources,
                           List_Source_Directories,
                           List_Binaries,
                           List_Binaries_Directories,
                           List_Sources_And_Projects,
                           List_Mains,
                           List_Projects,
                           List_Object_Paths,
                           List_Object_Zombies,
                           List_Source_Zombies,
                           List_Sources_That_Have_Visibility_On,
                           Version);

   type T_Action_Value is array (T_Action_Kinds'Range) of Unbounded_String;

   Action                : T_Action_Kinds   := Help;
   Action_Value          : T_Action_Value   := (others => Null_Unbounded_String);
   Transitive_Analysis   : Boolean          := True;
   Exclude_Externally    : Boolean          := False;
   Exclude_Library_Based : Boolean          := False;
   With_Entity           : Unbounded_String := Null_Unbounded_String;
   Asis_Tree_Files       : Boolean          := False;
   Additional_Unit       : Unbounded_String := Null_Unbounded_String;
   Output_File_Name      : Unbounded_String := Null_Unbounded_String;
   Use_Output_File       : Boolean          := False;
   Output_Handler        : Ada.Text_IO.File_Access;
   Output_File           : aliased Ada.Text_IO.File_Type;
   Base_Path             : Unbounded_String := Null_Unbounded_String;
   Relocate_Path         : Unbounded_String := Null_Unbounded_String;
   procedure Analyse_Options;
   -- Analyses and sets program options

   procedure Help_Options;
   -- Help on command line options
   procedure User_Message (Message : String);

end GNAT_GPR.Options;
