--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2020, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

-- Ada
with Ada.Containers.Ordered_Maps,
     Ada.Containers.Indefinite_Ordered_Sets,
     Ada.Characters.Handling,
     Ada.Directories,
     Ada.Text_IO;

with Ada.Command_Line,
     Ada.Containers.Hashed_Sets,
     Ada.Containers.Indefinite_Vectors,
     Ada.Strings.Fixed,
     Ada.Strings.Hash;

use Ada.Directories;

-- GPR
with GPR,
     GPR.Tempdir,
     GPR.Snames;

-- GNAT
with GNAT.String_Split,
     GNAT.Strings,
     GNAT.OS_Lib;

with GNATCOLL.VFS,
     GNATColl.Projects.Internals;

with GNAT_GPR.Options;

use GNAT_GPR.Options;

--  LAL
with Langkit_Support.Text;

with Libadalang.Analysis,
     Libadalang.Auto_Provider,
     Libadalang.Common,
     Libadalang.Iterators;

--  LAL
package body GNAT_GPR is

   package String_Sets is new Ada.Containers.Indefinite_Ordered_Sets (String);

   function US_Hash (U : Unbounded_String) return Ada.Containers.Hash_Type is
   begin
      return Ada.Strings.Hash (To_String (U));
   end US_Hash;

   -- --------------------------------------------------------------------------
   package String_Containers is new Ada.Containers.Hashed_Sets (Element_Type        => Unbounded_String,
                                                                Hash                => US_Hash,
                                                                Equivalent_Elements => "=",
                                                                "="                 => "=");

   Env      : GNATCOLL.Projects.Project_Environment_Access := null;
   Tree     : GNATCOLL.Projects.Project_Tree;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Image (Node : Libadalang.Analysis.Ada_Node) return String
   --  as from GNAT 2021, this function can be removed and Node'Image can be use direclty
   --
   is
   begin
      return Langkit_Support.Text.Image (Node.Text);
   end Image;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Error_Handler (Project    : GPR.Project_Id;
                            Is_Warning : Boolean)
   is
   begin
      null;
   end Error_Handler;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Equal (Left  : T_Directory_Information;
                   Rigth : T_Directory_Information)
                   return Boolean
   is
   begin
      return Left.Directory_Name = Rigth.Directory_Name;
   end Equal;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Equals (Left  : T_Unit_Information;
                    Right : T_Unit_Information)
                    return Boolean
   is
   begin
      return Left.Parent_GPR_Name = Right.Parent_GPR_Name and then Left.Unit_File_Name = Right.Unit_File_Name;
   end;

   -------------
   -- Is_Less --
   -------------

   function Is_Less (L, R : T_Unit_Information) return Boolean is
   begin
      return Ada.Directories.Simple_Name (To_String (L.Unit_File_Name))'Length < Ada.Directories.Simple_Name (To_String (R.Unit_File_Name))'Length;
   end Is_Less;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Load_GPR (Name       : in     String;
                       Project    :    out T_Project)
   is
      use GNATCOLL.Projects,
          GNATCOLL.VFS;
   begin
      Load (Self              => Tree,
            Root_Project_Path => Create (Full_Filename => +Name,
                                         Host          => Local_Host,
                                         Normalize     => True),
            Env               => Env,
            Packages_To_Check => No_Packs,
            Errors            => null,
            Recompute_View    => True);
      Project.Handler := Root_Project (Tree);
      null;
   end Load_GPR;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   protected Handler
   is
      procedure Set_Path_From_Gnatls (Gnatls       :        GNAT.Strings.String_Access;
                                      GNAT_Version :    out GNAT.Strings.String_Access);
   end Handler;

   protected body Handler
   is
      procedure Set_Path_From_Gnatls (Gnatls       :        GNAT.Strings.String_Access;
                                      GNAT_Version :    out GNAT.Strings.String_Access)
      is
      begin
         GNATColl.Projects.Set_Path_From_Gnatls
           (Self         => Env.all,
            Gnatls       => Gnatls.all,
            GNAT_Version => GNAT_Version,
            Errors       => null);
      end Set_Path_From_Gnatls;
   end Handler;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Initialize
   is
      Gnatls       : GNAT.Strings.String_Access := GNAT.OS_Lib.Locate_Exec_On_Path ("gnatls");
      GNAT_Version : GNAT.Strings.String_Access;
   begin
      if not Init_Output_Done then
         Init_Output_Done := True;
         if Use_Output_File then
            declare
               File : constant String := To_String (Output_File_Name);
               Dir  : constant String := Ada.Directories.Containing_Directory (File);
            begin
               if not Ada.Directories.Exists (Dir) then
                  Ada.Directories.Create_Path (Dir);
               elsif Ada.Directories.Exists (File) then
                  Ada.Directories.Delete_File (File);
               end if;
               Ada.Text_IO.Create (File => Output_File,
                                   Mode => Ada.Text_IO.Out_File,
                                   Name => File);
               Output_Handler := Output_File'Access;
            end;
         else
            Output_Handler := Ada.Text_IO.Standard_Output;
         end if;
      end if;

      GPR.Tempdir.Use_Temp_Dir (Status => True);
      GPR.Snames.Initialize;

      GNATColl.Projects.Internals.Initialize (Error_Handler'Access,
                                              Env);

      GNATColl.Projects.Set_Disable_Use_Of_TTY_Process_Descriptor (Env.all,
                                                                   Disabled => True);
      --  this avoids strange freeze,
      --  or errors like "[error]: cannot allocate slave side of the pty"
      --

      Handler.Set_Path_From_Gnatls (Gnatls       => Gnatls,
                                    GNAT_Version => GNAT_Version);


      GNAT.Strings.Free (Gnatls);
      GNAT.Strings.Free (GNAT_Version);

   end Initialize;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Finalize is
   begin
      GNATCOLL.Projects.Finalize;
   end Finalize;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Display_Project_Sources (Container                  : in Containers.Map;
                                      With_Corresponding_Project : in Boolean;
                                      With_Unit_Kind             : in Boolean)
   is
   begin
      for Unit of Container loop
         if With_Corresponding_Project and With_Unit_Kind then
            Ada.Text_IO.Put_Line (Output_Handler.all,
                                  To_String (Unit.Unit_File_Name) & ":" &
                                    To_String (Unit.Parent_GPR_Name));
         elsif With_Corresponding_Project and not With_Unit_Kind then
            Ada.Text_IO.Put_Line (Output_Handler.all,
                                  To_String (Unit.Unit_File_Name) & ":" & To_String (Unit.Parent_GPR_Name));
         elsif not With_Corresponding_Project and With_Unit_Kind then
            Ada.Text_IO.Put_Line (Output_Handler.all,
                                  To_String (Unit.Unit_File_Name));
         else
            Ada.Text_IO.Put_Line (Output_Handler.all,
                                  To_String (Unit.Unit_File_Name));
         end if;
      end loop;
   end Display_Project_Sources;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Display_Project_Source_Directories (Container : in Containers.Map)
   is
      Paths_Container      : String_Containers.Set;
      -- -----------------------------------------------------------------------
      procedure Sort_Unique (Cursor : in Containers.Cursor) is
         Element : constant Unbounded_String
           := To_Unbounded_String (Ada.Directories.Containing_Directory
                                   (To_String (Containers.Element (Cursor).Unit_File_Name)));
      begin
         if not Paths_Container.Contains (Element) then
            Paths_Container.Include (Element);
         end if;
      end Sort_Unique;
      procedure Display_Source_Directories (Cursor : in String_Containers.Cursor) is
      begin
         Ada.Text_IO.Put_Line
           (Output_Handler.all,
            To_String (String_Containers.Element (Cursor)));
      end Display_Source_Directories;
   begin
      Container.Iterate (Sort_Unique'Access);
      Paths_Container.Iterate (Display_Source_Directories'Access);
   end Display_Project_Source_Directories;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Display_Project_GPR (Container : in Containers.Map)
   is
   begin
      for P of Container loop
         Ada.Text_IO.Put_Line (Output_Handler.all,
                               To_String (P.Parent_GPR_Name));
      end loop;
   end Display_Project_GPR;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Display_Project_Executable_Path (Project : in T_Project)
   is
   begin
      Ada.Text_IO.Put_Line (Output_Handler.all,
                            Get_Project_Executable_Path (Project));
   end Display_Project_Executable_Path;

  -- --------------------------------------------------------------------------
   --
  -- --------------------------------------------------------------------------

   procedure Display_Project_Executables (Project            : in T_Project;
                                          Including_Others   : in Boolean)
   is
      Files : Containers.Map;
      -- -----------------------------------------------------------------------
      procedure Process (Cursor : in Containers.Cursor) is
      begin
         Ada.Text_IO.Put_Line
           (Output_Handler.all,
            To_String (Containers.Element (Cursor).Unit_File_Name));
      end Process;
   begin
      Set_Project_Executables
        (Project,
         Files,
         Including_Others);
      Files.Iterate (Process'Access);
   end Display_Project_Executables;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Display_Project_Object_Paths (Project             : in T_Project;
                                           Recursive           : in Boolean;
                                           Including_Libraries : in Boolean;
                                           Exclude_Externally  : in Boolean)
   is
      Objects_Paths : Directory_Info_Container.Vector;
   begin
      Set_Project_Object_Paths (Project,
                                Objects_Paths,
                                Recursive,
                                Including_Libraries,
                                Exclude_Externally);

      for Path of Objects_Paths loop
         Ada.Text_IO.Put_Line (Output_Handler.all,
                               To_String (Path.Directory_Name));
      end loop;
   end Display_Project_Object_Paths;

   package Duplicated_File_Container is new Ada.Containers.Ordered_Maps (Key_Type     => Unbounded_String,-- Simple_Name
                                                                         Element_Type => String_Containers.Set,
                                                                         "="          => String_Containers."=");

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Display_Project_Object_File_Zoombies (Project             : in String;
                                                   Transitive_Analysis : in Boolean;
                                                   Exclude_Externally  : in Boolean)
   is
      Result : GNAT_GPR.Containers.Map := Containers.Empty_Map;
   begin
      Set_Project_Object_File_Zoombies (Project             => Project,
                                        Container           => Result,
                                        Transitive_Analysis => Transitive_Analysis,
                                        Exclude_Externally  => Exclude_Externally);

      for R of Result loop
         Ada.Text_IO.Put_Line (Output_Handler.all,
                               To_String (R.Unit_File_Name));
      end loop;
   end Display_Project_Object_File_Zoombies;

      -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_Source_File_Zoombies (Project               : in     String;
                                               Container             :    out Containers.Map;
                                               Transitive_Analysis   : in     Boolean;
                                               Exclude_Externally    : in     Boolean;
                                               Exclude_Library_Based : in     Boolean)
   is
      All_Sources            : GNAT_GPR.Containers.Map := Containers.Empty_Map;
      Current_Project        : T_Project;
      Mains                  : GNAT_GPR.Containers.Map := Containers.Empty_Map;
      --
      LAL_Auto_Unit_Provider : Libadalang.Auto_Provider.Auto_Unit_Provider;
      LAL_Context            : Libadalang.Analysis.Analysis_Context;
      Unit_Provider_Ref      : Libadalang.Analysis.Unit_Provider_Reference;
      --
      Processed_Units        : GNAT_GPR.Containers.Map := Containers.Empty_Map;
      New_Units              : GNAT_GPR.Containers.Map := Containers.Empty_Map;
      Excluded_Units         : GNAT_GPR.Containers.Map := Containers.Empty_Map;
      Subunits               : GNAT_GPR.Containers.Map := Containers.Empty_Map;
      --

      -------------
      -- To_Spec_File --
      -------------

      function To_Spec_File (S : String) return String
      is
      begin
         return Ada.Characters.Handling.To_Lower (GNAT_GPR.Replace (Char => '.',
                                                                    To   => "-",
                                                                    Item => S)) & ".ads";
      end To_Spec_File;

      -------------
      -- To_Body_File --
      -------------

      function To_Body_File (S : String) return String
      is
      begin
         return Ada.Characters.Handling.To_Lower (GNAT_GPR.Replace (Char => '.',
                                                                    To   => "-",
                                                                    Item => S)) & ".adb";
      end To_Body_File;

      -------------
      -- Add_Unit --
      -------------

      procedure Add_Unit (Unit : String)
      is
         Formatted : constant String := Ada.Characters.Handling.To_Upper (Unit);
      begin
         if Ada.Strings.Fixed.Index (Source  => Formatted,
                                     Pattern => "ADA.") /= 1 and
           Ada.Strings.Fixed.Index (Source  => Formatted,
                                    Pattern => "GNAT.") /= 1 and
           Ada.Strings.Fixed.Index (Source  => Formatted,
                                    Pattern => "SYSTEM.") /= 1 and
           Ada.Strings.Fixed.Index (Source  => Formatted,
                                    Pattern => "INTERFACES.") /= 1 and
           Ada.Strings.Fixed.Index (Source  => Formatted,
                                    Pattern => "STANDARD.") /= 1
         then
            if not New_Units.Contains (To_Spec_File (Formatted)) then
               if not Excluded_Units.Contains (Formatted) then
                  if All_Sources.Contains (To_Spec_File (Formatted)) then
                     if not New_Units.Contains (To_Spec_File (Formatted)) and then
                       not Processed_Units.Contains (To_Spec_File (Formatted))
                     then

                        New_Units.Insert (To_Spec_File (Formatted),
                                          All_Sources.Element (To_Spec_File (Formatted)));

                        if All_Sources.Contains (To_Body_File (Formatted)) and then
                          not New_Units.Contains (To_Body_File (Formatted)) and then
                          not Processed_Units.Contains (To_Body_File (Formatted))
                        then
                           New_Units.Insert (To_Body_File (Formatted),
                                             All_Sources.Element (To_Body_File (Formatted)));
                        end if;
                     end if;
                  elsif All_Sources.Contains (To_Body_File (Formatted)) then
                     if not New_Units.Contains (To_Body_File (Formatted)) and then
                       not Processed_Units.Contains (To_Body_File (Formatted))
                     then
                        New_Units.Insert (To_Body_File (Formatted),
                                          All_Sources.Element (To_Body_File (Formatted)));
                     end if;
                  else
                     --  The unit is on an excluded project
                     --
                     null;
                     Excluded_Units.Insert (Formatted,
                                            K_Null_Unit_Information);
                  end if;
               end if;
            end if;
         end if;
      end Add_Unit;

      -------------
      -- Process --
      -------------

      procedure Process (N : Libadalang.Analysis.Ada_Node)
      is
         With_Clause    : constant Libadalang.Analysis.With_Clause := Libadalang.Analysis.As_With_Clause (N);
         Status         :          Boolean                         := False;
         R              :          Libadalang.Analysis.Ada_Node;
         use type Libadalang.Common.Ada_Node_Kind_Type;


      begin
         for I in With_Clause.First_Child_Index .. With_Clause.Last_Child_Index loop
            With_Clause.Get_Child (Index           => I,
                                   Index_In_Bounds => Status,
                                   Result          => R);
            if Status then
               if R.Kind = Libadalang.Common.Ada_Identifier then
                  Add_Unit (Image (R));
               elsif R.Kind = Libadalang.Common.Ada_Name_List then
                  declare
                     Name_List : constant Libadalang.Analysis.Name_List := Libadalang.Analysis.As_Name_List (R);
                     Status    :          Boolean                       := False;
                     R         :          Libadalang.Analysis.Ada_Node;
                  begin
                     for I in Name_List.First_Child_Index .. Name_List.Last_Child_Index loop
                        Name_List.Get_Child (Index           => I,
                                             Index_In_Bounds => Status,
                                             Result          => R);
                        if Status then
                           Add_Unit (Image (R));
                        end if;
                     end loop;
                  end;
               end if;
            end if;
         end loop;
      end Process;

      -------------
      -- Process_Subunit --
      -------------

      procedure Process_Subunit (N : Libadalang.Analysis.Ada_Node)
      is
      begin
         Subunits.Include (To_Unit (N.Unit.Get_Filename),
                           K_Null_Unit_Information);
      end Process_Subunit;

      -------------
      -- Is_Subunit --
      -------------

      function Is_Subunit (N : Libadalang.Analysis.Ada_Node) return Boolean
      is
         use type Libadalang.Common.Ada_Node_Kind_Type;
      begin
         if N.Kind = Libadalang.Common.Ada_Subunit then
            return True;
         else
            return False;
         end if;
      end Is_Subunit;

      -------------
      -- Is_Withed_Unit --
      -------------

      function Is_Withed_Unit (N : Libadalang.Analysis.Ada_Node) return Boolean
      is
         use type Libadalang.Common.Ada_Node_Kind_Type;
      begin
         if N.Kind = Libadalang.Common.Ada_With_Clause then
            return True;
         else
            return False;
         end if;
      end Is_Withed_Unit;

      -------------
      -- Do_Process --
      -------------

      procedure Do_Process (Unit : String;
                            Kind : Libadalang.Common.Analysis_Unit_Kind)
      is
         U                 : constant Libadalang.Analysis.Analysis_Unit'Class := LAL_Auto_Unit_Provider.Get_Unit
           (Context => LAL_Context,
            Name    => Langkit_Support.Text.To_Text (Unit),
            Kind    => Kind);
         Root              : constant Libadalang.Analysis.Ada_Node'Class := U.Root;
         Iterator          : Libadalang.Iterators.Traverse_Iterator'Class := Libadalang.Iterators.Find (Root, Is_Withed_Unit'Access);
      begin
         Iterator.Iterate (Process'Access);
      end Do_Process;

      -------------
      -- Do_Validate_Subunits_If_Parent_Withed --
      -------------

      procedure Do_Validate_Subunits_If_Parent_Withed (Unit : String)
      is
         U                 : constant Libadalang.Analysis.Analysis_Unit'Class := LAL_Auto_Unit_Provider.Get_Unit
           (Context => LAL_Context,
            Name    => Langkit_Support.Text.To_Text (Unit),
            Kind    => Libadalang.Common.Unit_Body);
         Root              : constant Libadalang.Analysis.Ada_Node'Class := U.Root;
         Iterator : Libadalang.Iterators.Traverse_Iterator'Class := Libadalang.Iterators.Find (Root, Is_Subunit'Access);
      begin
         Iterator.Iterate (Process_Subunit'Access);
      end Do_Validate_Subunits_If_Parent_Withed;
      -------------
      -- Get_With_Clause --
      -------------

      procedure Get_With_Clause (File    : String)
      is
         Unit : constant String := To_Unit (File);
      begin
         for Index in 0 .. Ada.Strings.Fixed.Count (Source => Unit, Pattern => ".") loop
            declare
               Current_Unit : constant String := Extract_Parent (Unit, Index);
            begin
               Do_Process (Unit => Current_Unit,
                           Kind => Libadalang.Common.Unit_Specification);
               Do_Process (Unit => Current_Unit,
                           Kind => Libadalang.Common.Unit_Body);
               Add_Unit (Current_Unit);
            end;
         end loop;
      end Get_With_Clause;

   begin
      GNAT_GPR.Load_GPR (Name    => Project,
                         Project => Current_Project);

      GNAT_GPR.Set_Project_Mains (Project             => Current_Project,
                                  Transitive_Analysis => Transitive_Analysis,
                                  Container           => Mains);

      GNAT_GPR.Set_Project_Source_Files (Project               => Current_Project,
                                         Container             => All_Sources,
                                         Recursive             => Transitive_Analysis,
                                         Exclude_Externally    => Exclude_Externally,
                                         Exclude_Library_Based => Exclude_Library_Based);

      Initiate_Libadalang_Context:
      declare
         Files      : GNATColl.VFS.File_Array (1 .. Natural (All_Sources.Length));
         File_Index : Positive := 1;
         VF         :          GNATCOLL.VFS.Virtual_File;
      begin
         for U of All_Sources loop
            VF                  := GNATCOLL.VFS.Create (Full_Filename => GNATCOLL.VFS."+" (To_String (U.Unit_File_Name)));
            Files (File_Index)  := VF;
            File_Index          := File_Index + 1;
         end loop;

         LAL_Auto_Unit_Provider := Libadalang.Auto_Provider.Create_Auto_Provider (Input_Files => Files);
         Unit_Provider_Ref      := LAL_Auto_Unit_Provider.Create_Unit_Provider_Reference;
         LAL_Context            := Libadalang.Analysis.Create_Context (Unit_Provider => Unit_Provider_Ref);
      end Initiate_Libadalang_Context;

      for M of Mains loop
         Get_With_Clause (File => To_String (M.Unit_File_Name));
      end loop;

      while not New_Units.Is_Empty loop
         declare
            Tmp : constant GNAT_GPR.Containers.Map := New_Units;
         begin
            for K in New_Units.Iterate loop
               if not Processed_Units.Contains (Containers.Key (K)) then
                  Processed_Units.Include (Containers.Key (K),
                                           Containers.Element (K));
               end if;
            end loop;
            New_Units.Clear;
            for U of Tmp loop
               Get_With_Clause (File => To_String (U.Unit_File_Name));
            end loop;
         end;
      end loop;

      for K in All_Sources.Iterate loop
         if not Processed_Units.Contains (Containers.Key (K)) then
            Do_Validate_Subunits_If_Parent_Withed (Unit => To_Unit (Containers.Key (K)));
            if Subunits.Contains (To_Unit (Containers.Key (K))) and then
              Processed_Units.Contains (To_Spec_File (Extract_Parent (To_Unit (Containers.Key (K)), 1)))
            then
               --  This subunit parent is withed by the project, so he is inherently withed
               --
               null;
            else
               Container.Include (Containers.Key (K),
                                  Containers.Element (K));
            end if;
         end if;
      end loop;
      --  TODO
      --      - m is not given in a transitive manner, and dont work with aggregate
      --
   end Set_Project_Source_File_Zoombies;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_Object_File_Zoombies (Project             : in     String;
                                               Container           :    out Containers.Map;
                                               Transitive_Analysis : in     Boolean;
                                               Exclude_Externally  : in     Boolean)
   is
      Projects        : Containers.Map;
      Current_Project : T_Project;
   begin
      if Transitive_Analysis then
         GNAT_GPR.Load_GPR (Name    => Project,
                            Project => Current_Project);
         Set_Project_GPRs (Project            => Current_Project,
                           Container          => Projects,
                           Exclude_Externally => Exclude_Externally,
                           Recursive          => Transitive_Analysis);
         Finalize;
      end if;
      Projects.Include (Project,
                       (Unit_File_Name => Null_Unbounded_String,
                        Parent_GPR_Name => To_Unbounded_String (Project)));

      for P of Projects loop

         GNAT_GPR.Load_GPR (Name    => To_String (P.Parent_GPR_Name),
                            Project => Current_Project);

         Per_Project_Analyzis:
         declare
            Slice_Set            : GNAT.String_Split.Slice_Set;
            Slice_Number         : GNAT.String_Split.Slice_Number := GNAT.String_Split.Slice_Number'First;
            Project_Object_Paths : constant String := Get_Project_Object_Paths (Project             => Current_Project,
                                                                                Including_Libraries => True,
                                                                                Recursive           => False,
                                                                                Exclude_Externally  => Exclude_Externally,
                                                                                Prepend             => "",
                                                                                Postpend            => GNAT.OS_Lib.Path_Separator & "");
            Objects_Path         : String_Containers.Set;
            All_Files            : Duplicated_File_Container.Map;
            Searched_Objects     : Ada.Directories.Search_Type;
            Dir_Entry            : Ada.Directories.Directory_Entry_Type;

            use type GNAT.String_Split.Slice_Number;

            -- ----------
            --
            -- ----------

            procedure Search_Files (Position : String_Containers.Cursor)
            is
            begin
               Ada.Directories.Start_Search (Search      => Searched_Objects,
                                             Directory   => To_String (String_Containers.Element (Position)),
                                             Pattern     => "{*.o,*.ali}",
                                             Filter      => (Ada.Directories.Ordinary_File => True, others => False));

               while Ada.Directories.More_Entries (Searched_Objects) loop
                  Ada.Directories.Get_Next_Entry (Searched_Objects, Dir_Entry);

                  declare
                     Key       : constant Unbounded_String := To_Unbounded_String (Ada.Directories.Simple_Name (Dir_Entry));
                     Full_Name : constant Unbounded_String := To_Unbounded_String (Ada.Directories.Full_Name (Dir_Entry));

                     -- ----------
                     --
                     -- ----------

                     procedure Update (Key     : in     Unbounded_String;
                                       Element : in out String_Containers.Set)
                     is
                        pragma Unreferenced (Key);
                     begin
                        Element.Include (Full_Name);
                     end;
                  begin
                     if All_Files.Contains (Key) then
                        All_Files.Update_Element (Position => All_Files.Find (Key),
                                                  Process  => Update'Access);
                     else
                        declare
                           Files  : String_Containers.Set;
                        begin
                           Files.Include (Full_Name);
                           All_Files.Insert  (Key, Files);
                        end;
                     end if;
                  end;
               end loop;
               Ada.Directories.End_Search (Searched_Objects);

            end Search_Files;

            -- ----------
            --
            -- ----------

            procedure Show_Zoombies (Position : Duplicated_File_Container.Cursor)
            is

               -- ----------
               --
               -- ----------

               procedure Display (Position : String_Containers.Cursor)
               is
                  Element : constant Unbounded_String := String_Containers.Element (Position);
                  U       :          T_Unit_Information;
                  Key     : constant String := Simple_Name (To_String (Element));
               begin
                  U.Unit_File_Name := Element;
                  if not Container.Contains (Key) then
                     Container.Include (Key,
                                        U);
                  end if;
               end;

               Object_Name : constant Unbounded_String := To_Unbounded_String (Ada.Directories.Base_Name (To_String
                                                                               (Duplicated_File_Container.Key (Position))) & ".o");
               ALI_Name    : constant Unbounded_String := To_Unbounded_String (Ada.Directories.Base_Name (To_String
                                                                               (Duplicated_File_Container.Key (Position))) & ".ali");
               use type Ada.Containers.Count_Type;
            begin
               if Duplicated_File_Container.Key (Position) = Object_Name and then
                 Duplicated_File_Container.Element (Position).Length >= 2 then
                  Duplicated_File_Container.Element (Position).Iterate (Display'Access);
                  if All_Files.Contains (ALI_Name) then
                     All_Files.Element (ALI_Name).Iterate (Display'Access);
                  end if;
               end if;
               -- we do not do the same for ali files as they are duplicated between objects and libraries
               --
            end Show_Zoombies;
            Sources_Container : GNAT_GPR.Containers.Map := Containers.Empty_Map;
         begin
            -- 1 : set Objects_Path
            GNAT.String_Split.Create (Slice_Set,
                                      Project_Object_Paths,
                                      GNAT.OS_Lib.Path_Separator & "");

            Slice_Number := GNAT.String_Split.Slice_Count (Slice_Set);

            if Slice_Number >= GNAT.String_Split.Slice_Number'First + 1 then

               for Index in GNAT.String_Split.Slice_Number'First + 1 .. Slice_Number loop

                  declare
                     Path : constant Unbounded_String := To_Unbounded_String (GNAT.String_Split.Slice
                                                                              (Slice_Set, GNAT.String_Split.Slice_Number (Index)));
                     use type String_Containers.Cursor;
                  begin
                     if Path /= "" and then Objects_Path.Find (Path) = String_Containers.No_Element then
                        Objects_Path.Include (Path);
                     end if;
                  end;
               end loop;
            end if;

            -- 2 : Search Files
            Objects_Path.Iterate (Search_Files'Access);
            -- 3 :
            All_Files.Iterate (Show_Zoombies'Access);

            GNAT_GPR.Set_Project_Source_Files (Project               => Current_Project,
                                               Container             => Sources_Container,
                                               Recursive             => False,
                                               Exclude_Externally    => False,
                                               Exclude_Library_Based => False);

            Filter_Binder_Sources:
            declare
               Mains : GNAT_GPR.Containers.Map := Containers.Empty_Map;
            begin
               GNAT_GPR.Set_Project_Mains (Project             => Current_Project,
                                           Transitive_Analysis => Transitive_Analysis,
                                           Container           => Mains);
               for Main of Mains loop
                  declare
                     Base   : constant String := Ada.Directories.Containing_Directory (To_String (Main.Unit_File_Name));
                     Name   : constant String := Ada.Directories.Simple_Name (To_String (Main.Unit_File_Name));
                     Binder :          T_Unit_Information := Main;
                  begin
                     Binder.Unit_File_Name := To_Unbounded_String (Base & GNAT.OS_Lib.Directory_Separator & "b__" & Name);
                     Sources_Container.Include ("b__" & Name,
                                                Binder);
                  end;
               end loop;
            end Filter_Binder_Sources;

            for F in All_Files.Iterate loop
               declare
                  Obj   : constant String   := To_String (Duplicated_File_Container.Key (F));
               begin
                  if (for all F of Sources_Container => Ada.Directories.Base_Name (To_String (F.Unit_File_Name)) /=
                        Ada.Directories.Base_Name (Obj)) then
                     for D in Duplicated_File_Container.Element (F).Iterate loop
                        declare
                           U   : constant T_Unit_Information := (Parent_GPR_Name => Null_Unbounded_String,
                                                                 Unit_File_Name  => String_Containers.Element (D));
                           Key : constant String             := To_String (U.Unit_File_Name);
                        begin
                           if not Container.Contains (Key) then
                              Container.Include (Key,
                                                 U);
                           end if;
                        end;
                     end loop;
                  end if;
               end;
            end loop;
         end Per_Project_Analyzis;
         Finalize;
      end loop;
   end Set_Project_Object_File_Zoombies;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Get_Project_Source_Paths (Project            : in T_Project;
                                      Exclude_Externally : in Boolean;
                                      Prepend            : in String;
                                      PostPend           : in String) return String is
      Project_Source_Paths : Unbounded_String := Null_Unbounded_String;
      Container            : Containers.Map;
      Paths_Container      : String_Containers.Set;

      -- -----------------------------------------------------------------------
      procedure Sort_Unique (Cursor : in Containers.Cursor) is
         Element : constant Unbounded_String := Containers.Element (Cursor).Unit_File_Name;
      begin
         if not Paths_Container.Contains (Element) then
            Paths_Container.Include (Element);
         end if;
      end Sort_Unique;

      -- -----------------------------------------------------------------------
      procedure Create_Project_Paths_String (Cursor : in String_Containers.Cursor) is
      begin
         Append (Source   => Project_Source_Paths,
                 New_Item => Prepend &
                 To_String (String_Containers.Element (Cursor)) &
                 PostPend);
      end Create_Project_Paths_String;
   begin
      Set_Project_Source_Directories (Project            => Project,
                                      Container          => Container,
                                      Recursive          => True,
                                      Exclude_Externally => Exclude_Externally);
      Container.Iterate (Sort_Unique'Access);
      Paths_Container.Iterate (Create_Project_Paths_String'Access);
      return To_String (Project_Source_Paths);
   end Get_Project_Source_Paths;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Get_Project_Object_Paths (Project             : in T_Project;
                                      Including_Libraries : in Boolean;
                                      Recursive           : in Boolean;
                                      Exclude_Externally  : in Boolean;
                                      Prepend             : in String;
                                      Postpend            : in String) return String
   is
      Result         : Unbounded_String;
      Directory_Info : Directory_Info_Container.Vector;
   begin
      Set_Project_Object_Paths
        (Project,
         Directory_Info,
         Recursive,
         Including_Libraries,
         Exclude_Externally);

      for Unit of Directory_Info loop
         Ada.Strings.Unbounded.Append
           (Result,
            Prepend &
              Unit.Directory_Name &
              Postpend);
      end loop;

      return To_String (Result);
   end Get_Project_Object_Paths;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Get_Project_Executable_Path (Project : in T_Project) return String
   is
      use GNATCOLL.Projects,
          GNATCOLL.VFS;
   begin
         return +Full_Name (Executables_Directory (Project.Handler));
   end Get_Project_Executable_Path;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_Executables (Project            : in      T_Project;
                                      Container          :     out Containers.Map;
                                      Including_Others   : in      Boolean)
   is
      pragma Unreferenced (Including_Others);
      use GNATCOLL.Projects,
          GNATCOLL.VFS;
      Unit : T_Unit_Information;
   begin
      Container.Clear;

      for Dir of File_And_Project_Array_Access'(Source_Files (Project   => Project.Handler,
                                                              Recursive => False)).all loop
         if Is_Main_File (Project        => Project.Handler,
                          File           => Base_Name (Dir.File),
                          Case_Sensitive => False) then
            declare
               Exe            : constant String := +Executable_Name (Project.Handler, Full_Name (Dir.File));
               Containing_Dir : constant String := +Full_Name (Executables_Directory (Project.Handler));
            begin
               Unit.Unit_File_Name := To_Unbounded_String
                 (GNAT.OS_Lib.Normalize_Pathname (Containing_Dir & GNAT.OS_Lib.Directory_Separator & Exe));
               Container.Include (Simple_Name (To_String (Unit.Unit_File_Name)),
                                  Unit);
            end;
         end if;
      end loop;
   end Set_Project_Executables;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_Object_Paths (Project             : in     T_Project;
                                       Objects_Paths       :    out Directory_Info_Container.Vector;
                                       Recursive           : in     Boolean;
                                       Including_Libraries : in     Boolean;
                                       Exclude_Externally  : in     Boolean)
   is
      use GNATCOLL.VFS;
      Unit : T_Directory_Information;
   begin
      Objects_Paths.Clear;

      for File of GNATCOLL.Projects.Object_Path (Project             => Project.Handler,
                                                 Recursive           => Recursive,
                                                 Including_Libraries => Including_Libraries,
                                                 Xrefs_Dirs          => False,
                                                 Exclude_Externally  => Exclude_Externally) loop
         Unit.Directory_Name := To_Unbounded_String (+Full_Name (File));
         if Unit.Directory_Name /= "" and not Objects_Paths.Contains (Unit) then
            Objects_Paths.Append (Unit);
         end if;
      end loop;

   end Set_Project_Object_Paths;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_Source_Files (Project               : in     T_Project;
                                       Container             : in out Containers.Map;
                                       Recursive             : in     Boolean;
                                       Exclude_Externally    : in     Boolean;
                                       Exclude_Library_Based : in     Boolean;
                                       Body_Only             : in     Boolean := False)
   is
      use GNATCOLL.Projects,
          GNATCOLL.VFS;
      Unit : T_Unit_Information;
      Ext  : String (1 .. 3) := "---";
      Base : Unbounded_String;
      Pos  : Containers.Cursor;
   begin
      Container.Clear;

      for Dir of Internals.Source_Files (Project               => Project.Handler,
                                         Recursive             => Recursive,
                                         Exclude_Externally    => Exclude_Externally,
                                         Exclude_Library_Based => Exclude_Library_Based).all loop
         if Body_Only then
            declare
               N : constant String:= +Full_Name (Dir.file);
            begin
               Ext  := Ada.Characters.Handling.To_Lower (N (N'Last - 2 .. N'Last));
               Base := To_Unbounded_String (N (N'First .. N'Last - 4));
            end;
         end if;

         Unit.Unit_File_Name  := To_Unbounded_String (+Full_Name (Dir.file));
         Unit.Parent_GPR_Name := To_Unbounded_String (+Full_Name (Dir.Project.Project_Path));

         if not Body_Only then
            if not Container.Contains (Simple_Name (To_String (Unit.Unit_File_Name))) then
               Container.Include (Simple_Name (To_String (Unit.Unit_File_Name)),
                                 Unit);
            end if;
         elsif Body_Only then

            if Ext = "ads" then
               Pos := Container.Find (Simple_Name (To_String (Base) & ".adb"));
               if not Containers.Has_Element (Pos) then
                  if not Container.Contains (Simple_Name (To_String (Unit.Unit_File_Name))) then
                     Container.Include (Simple_Name (To_String (Unit.Unit_File_Name)),
                                        Unit);
                  end if;
               end if;
            elsif Ext = "adb" then
               Pos := Container.Find (Simple_Name (To_String (Base) & ".ads"));

               if Containers.Has_Element (Pos) then
                  Container.Delete (Pos);
               end if;
               if not Container.Contains (Simple_Name (To_String (Unit.Unit_File_Name))) then
                  Container.Include (Simple_Name (To_String (Unit.Unit_File_Name)),
                                     Unit);
               end if;
            end if;
         end if;
      end loop;
   end Set_Project_Source_Files;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_Source_Files_That_Have_Visibility_On (Unit                           : in     String;
                                                               Project                        : in     T_Project;
                                                               Recursive                      : in     Boolean;
                                                               Exclude_Externally             : in     Boolean;
                                                               Get_Asis_Tree_Files            : in     Boolean;
                                                               Include_Unit_Parents           : in     Boolean;
                                                               Additional_Unit                : in     String;
                                                               Container                      : in out Containers.Map)
   is
      Sources_Container            :         GNAT_GPR.Containers.Map := GNAT_GPR.Containers.Empty_Map;
      Directories_Container        :         GNAT_GPR.Containers.Map := GNAT_GPR.Containers.Empty_Map;
      All_Sources_Container        :         GNAT_GPR.Containers.Map := GNAT_GPR.Containers.Empty_Map;
      Units_That_Have_Visibilities :         String_Sets.Set         := String_Sets.Empty_Set;
      Relocate                     : constant String                 := To_String (Options.Relocate_Path);
      Root_Dir                     : constant String                 := To_String (Options.Base_Path);
      --
      LAL_Auto_Unit_Provider       :          Libadalang.Auto_Provider.Auto_Unit_Provider;
      VF                           :          GNATCOLL.VFS.Virtual_File;
      LAL_Context                  :          Libadalang.Analysis.Analysis_Context;
      Unit_Provider_Ref            :          Libadalang.Analysis.Unit_Provider_Reference;

      package Wide_String_Vectors is new Ada.Containers.Indefinite_Vectors (Positive, Wide_String);
      subtype String_Vector       is Wide_String_Vectors.Vector;

      package Object_Containers is new Ada.Containers.Indefinite_Ordered_Maps (Key_Type     => String, --  project
                                                                               Element_Type => String); --  object paths

      -------------
      -- Compute_Asis_Tree_Files --
      -------------

      procedure Compute_Asis_Tree_Files (Units     : in     GNAT_GPR.Containers.Map;
                                         Root_Dir  : in     String;
                                         Relocate  : in     String;
                                         Container : in out Containers.Map)
      is
         use Ada.Characters.Handling;
         Projects          : String_Vector;
         Units_Projects    : GNAT_GPR.Containers.Map;
         Unit_Information  : GNAT_GPR.T_Unit_Information;
         Objects           : Object_Containers.Map;
         P                 : GNAT_GPR.Containers.Cursor;

         -------------
         -- Add --
         -------------

         procedure Add (Key  : String;
                        Unit : T_Unit_Information)
         is
         begin
            Units_Projects.Include (Key,
                                    Unit);
            if not Projects.Contains (To_Wide_String (To_String (Unit.Parent_GPR_Name))) then
               Projects.Append (To_Wide_String (To_String (Unit.Parent_GPR_Name)));
            end if;
         end Add;
      begin
         for U in Units.Iterate loop
            declare
               Base     : constant String := GNAT_GPR.Containers.Key (U);
               File_Adb : constant String := Base (Base'First .. Base'Last - 1) & "b";
               File_Ads : constant String := Base (Base'First .. Base'Last - 1) & "s";
            begin
               P := All_Sources_Container.Find (File_Adb);
               if GNAT_GPR.Containers.Has_Element (P) then
                  Unit_Information := GNAT_GPR.Containers.Element (P);
                  Add (File_Adb, Unit_Information);
               else
                  P := All_Sources_Container.Find (File_Ads);
                  if GNAT_GPR.Containers.Has_Element (P) then
                     Unit_Information := GNAT_GPR.Containers.Element (P);
                     Add (File_Ads, Unit_Information);
                  end if;
               end if;
            end;
         end loop;

         for P of Projects loop
            declare
               Dirs    : GNAT_GPR.Directory_Info_Container.Vector;
               Project : T_Project;
            begin
               GNAT_GPR.Initialize;

               GNAT_GPR.Load_GPR (Name    => To_String (P),
                                  Project => Project);

               GNAT_GPR.Set_Project_Object_Paths (Project             => Project,
                                                  Objects_Paths       => Dirs,
                                                  Recursive           => False,
                                                  Including_Libraries => False,
                                                  Exclude_Externally  => True);

               GNAT_GPR.Finalize;

               Objects.Insert (To_String (P),
                               Replace (From => Root_Dir,
                                        To   => Relocate,
                                        Item => To_String (Dirs.First_Element.Directory_Name)));
            end;
         end loop;

         for U of Units_Projects loop
            declare
               Base            : constant String := Ada.Directories.Base_Name (To_String (U.Unit_File_Name));
               Unit_File_Name  : constant String := Objects.Element (To_String (U.Parent_GPR_Name)) &
                 To_Lower (GNAT_GPR.Replace (Char => '.',
                                             To   => "-",
                                             Item => Base)) & ".adt";
            begin
               Container.Include (Key      => Simple_Name (Unit_File_Name),
                                  New_Item => (Unit_File_Name  => To_Unbounded_String (Unit_File_Name),
                                               Parent_GPR_Name => U.Unit_File_Name));
            end;
         end loop;

      end Compute_Asis_Tree_Files;

      -------------
      -- Append_To_Container --
      -------------

      procedure Append_To_Container (U : T_Unit_Information)
      is
         File      : constant String := Ada.Directories.Simple_Name (To_String (U.Unit_File_Name));
         File_Base : constant String := File (File'First .. File'Last - 4);
      begin
         -- skip ads when adb is present
         -- remove ads when adb is added
         --
         if File (File'Last - 3 .. File'Last) = ".ads" then
            --  we are currently dealing with an ads
            --
            if Container.Contains (File_Base & ".adb") then
               -- but the corresponding adb is already present
               --
               null;
            else
               --  and the corresponding (potential) adb is not present
               --
               if not Container.Contains (File_Base & ".ads") then
                  --  as the ads is not present
                  --
                  if All_Sources_Container.Contains (File_Base & ".adb") then
                     --  and the adb exists in the sources, we add the adb instead
                     --
                     Container.Include (File_Base & ".adb",
                                        Sources_Container.Element (File_Base & ".adb"));
                     Units_That_Have_Visibilities.Include (Ada.Characters.Handling.To_Upper (To_Unit (File_Base)));
                  else
                     --  and the adb does not exists, we add the ads
                     --
                     Container.Include (File_Base & ".ads",
                                        U);
                     Units_That_Have_Visibilities.Include (Ada.Characters.Handling.To_Upper (To_Unit (File_Base)));
                  end if;
               end if;
            end if;
         else
            if Container.Contains (File_Base & ".ads") then
               --  as the adb exists but the ads is already added
               --
               Container.Delete (File_Base & ".ads");
               Container.Include (File_Base & ".adb",
                                  U);
            else
               if not Container.Contains (File_Base & ".adb") then
                  Container.Include (File_Base & ".adb",
                                     U);
                  Units_That_Have_Visibilities.Include (Ada.Characters.Handling.To_Upper (To_Unit (File_Base)));
               end if;
            end if;
         end if;
      end Append_To_Container;

      -- --------------------------------------------------------------------------
      --
      -- --------------------------------------------------------------------------

      -------------
      -- LAL_With_Clause --
      -------------

      procedure LAL_With_Clause (File    : String;
                                 Name    : String;
                                 Project : String;
                                 Unit    : String)
      is
         -------------
         -- Is_Withed_Unit --
         -------------

         function Is_Withed_Unit (N : Libadalang.Analysis.Ada_Node) return Boolean
         is
            use type Libadalang.Common.Ada_Node_Kind_Type;
         begin
            if N.Kind = Libadalang.Common.Ada_With_Clause then
               return True;
            else
               return False;
            end if;
         end Is_Withed_Unit;

         -------------
         -- Check_Visibility --
         -------------

         procedure Check_Visibility (Name           : in     String;
                                     File           : in     String;
                                     Project        : in     String;
                                     Withed_Unit    : in     String;
                                     Has_Visibility :    out Boolean)
         is
            Current   : constant String := Ada.Characters.Handling.To_Upper (Withed_Unit);
            Reference : constant String := Ada.Characters.Handling.To_Upper (Unit);
            Meta      :          T_Unit_Information;
         begin
            Has_Visibility := False;
            if Current = Reference or else
              (Current'Length > Reference'Length and then
               Ada.Strings.Fixed.Index (Source  => Current,
                                        Pattern => Reference & ".") = Current'First) then
               --  childs are also included in visibility
               --
               Meta.Unit_File_Name  := To_Unbounded_String (File);
               Meta.Parent_GPR_Name := To_Unbounded_String (Project);

               if not Container.Contains (Simple_Name (File)) then
                  Has_Visibility := True;
                  Append_To_Container (Meta);
                  Units_That_Have_Visibilities.Include (Ada.Characters.Handling.To_Upper (Name));

                  Add_Childs:
                  declare
                     Base : constant String := Ada.Directories.Base_Name (File);
                  begin
                     for S of Sources_Container loop
                        if Ada.Strings.Fixed.Index (Source  => Ada.Directories.Base_Name (To_String (S.Unit_File_Name)),
                                                    Pattern => Base & "-") = 1 then
                           Append_To_Container (S);
                           Units_That_Have_Visibilities.Include (Ada.Characters.Handling.To_Upper (To_Unit (To_String (S.Unit_File_Name))));
                        end if;
                     end loop;
                  end Add_Childs;
               end if;
            end if;
         end Check_Visibility;

         -------------
         -- Process --
         -------------

         procedure Process (N : Libadalang.Analysis.Ada_Node)
         is
            With_Clause    : constant Libadalang.Analysis.With_Clause := Libadalang.Analysis.As_With_Clause (N);
            Status         :          Boolean                         := False;
            Has_Visibility :          Boolean                         := False;
            R              :          Libadalang.Analysis.Ada_Node;
            use type Libadalang.Common.Ada_Node_Kind_Type;
         begin
            for I in With_Clause.First_Child_Index .. With_Clause.Last_Child_Index loop
               With_Clause.Get_Child (Index           => I,
                                      Index_In_Bounds => Status,
                                      Result          => R);
               if Status then
                  if R.Kind = Libadalang.Common.Ada_Identifier then
                     --  do we really need this ?
                     --
                     Check_Visibility (Name           => Name,
                                       File           => File,
                                       Project        => Project,
                                       Withed_Unit    => Image (R),
                                       Has_Visibility => Has_Visibility);
                     if Has_Visibility then
                        return;
                     end if;
                  elsif R.Kind = Libadalang.Common.Ada_Name_List then
                     declare
                        Name_List : constant Libadalang.Analysis.Name_List := Libadalang.Analysis.As_Name_List (R);
                        Status    :          Boolean                       := False;
                        R         :          Libadalang.Analysis.Ada_Node;
                     begin
                        for I in Name_List.First_Child_Index .. Name_List.Last_Child_Index loop
                           Name_List.Get_Child (Index           => I,
                                                Index_In_Bounds => Status,
                                                Result          => R);
                           if Status then
                              Check_Visibility (Name           => Name,
                                                File           => File,
                                                Project        => Project,
                                                Withed_Unit    => Image (R),
                                                Has_Visibility => Has_Visibility);
                              if Has_Visibility then
                                 return;
                              end if;
                           end if;
                        end loop;
                     end;
                  end if;
               end if;
            end loop;
         end Process;
      begin
         if File (File'Last - 3 .. File'Last) = ".ads" then
            declare
               U : constant Libadalang.Analysis.Analysis_Unit'Class := LAL_Auto_Unit_Provider.Get_Unit
                 (Context => LAL_Context,
                  Name    => Langkit_Support.Text.To_Text (Name),
                  Kind    => Libadalang.Common.Unit_Specification);
               Root : constant Libadalang.Analysis.Ada_Node'Class := U.Root;
            begin
               declare
                  Iterator : Libadalang.Iterators.Traverse_Iterator'Class := Libadalang.Iterators.Find (Root, Is_Withed_Unit'Access);
               begin
                  Iterator.Iterate (Process'Access);
               end;
            end;
         else
            declare
               U    : constant Libadalang.Analysis.Analysis_Unit'Class := LAL_Auto_Unit_Provider.Get_Unit
                 (Context => LAL_Context,
                  Name    => Langkit_Support.Text.To_Text (Name),
                  Kind    => Libadalang.Common.Unit_Body);
               Root : constant Libadalang.Analysis.Ada_Node'Class := U.Root;
            begin
               declare
                  Iterator : Libadalang.Iterators.Traverse_Iterator'Class := Libadalang.Iterators.Find (Root, Is_Withed_Unit'Access);
               begin
                  Iterator.Iterate (Process'Access);
               end;
            end;
         end if;
      end LAL_With_Clause;

      -------------
      -- Visibility_Analyzis --
      -------------

      procedure Visibility_Analyzis (Unit : String)
      is

         -------------
         -- Process_Unit --
         -------------

         procedure Process_Unit (Name    : Wide_String;
                                 File    : String;
                                 Project : String)
         is
            Meta : T_Unit_Information;
         begin
            Meta.Unit_File_Name  := To_Unbounded_String (File);
            Meta.Parent_GPR_Name := To_Unbounded_String (Project);

            if not Container.Contains (Simple_Name (File)) then
               LAL_With_Clause (File    => To_String (Meta.Unit_File_Name),
                                Name    => To_Unit (To_String (Meta.Unit_File_Name)),
                                Project => Project,
                                Unit    => Unit);

               declare
                  use Ada.Characters.Handling;
                  Current   : constant String := To_Upper (To_String (Name));
                  Reference : constant String := To_Upper (Unit);
               begin
                  if Current = Reference or else
                    (Current'Length > Reference'Length and then
                     Ada.Strings.Fixed.Index (Source  => Current,
                                              Pattern => Reference & ".") = Current'First) then
                     --  childs are also included in visibility
                     --
                     if not Container.Contains (Simple_Name (File)) then
                        Append_To_Container (Meta);
                        Units_That_Have_Visibilities.Include (Current);
                     end if;
                  end if;
               end;
            end if;
         end Process_Unit;

         -------------
         -- Parents_Has_Visibility --
         -------------

         function Parents_Has_Visibility (Unit : String) return Boolean
         is
            Count  : Natural := 0;
            Result : Boolean := False;
         begin
            if Unit = "" then
               Result := False;
            else
               Count := Ada.Strings.Fixed.Count (Source  => Unit,
                                                 Pattern => ".");
               if Count in Positive then
                  for Index in Count .. 1 loop
                     if Units_That_Have_Visibilities.Contains (Ada.Characters.Handling.To_Upper (Extract_Parent (Unit, Index))) then
                        Result := True;
                     end if;
                  end loop;
               end if;
               if not Result then
                  Result := Units_That_Have_Visibilities.Contains (Ada.Characters.Handling.To_Upper (Unit));
               end if;
            end if;
            return Result;
         end Parents_Has_Visibility;



         package Sort_By_Parent_Units_Ascending is new GNAT_GPR.Vector_Containers.Generic_Sorting (Is_Less);

         Sorted_Sources_Container : Vector_Containers.Vector;
      begin

         for U of Sources_Container loop
            Sorted_Sources_Container.Append (U);
         end loop;


         Sort_By_Parent_Units_Ascending.Sort (Sorted_Sources_Container);
         -- we will start by analysing the roots units
         -- so that we will be able to reduce the analysis as a child inherit from the visibility
         -- on its parent units
         --
         for S of Sorted_Sources_Container loop
            if Parents_Has_Visibility (Ada.Characters.Handling.To_Upper (To_Unit (To_String (S.Unit_File_Name)))) then
               -- we check if one of the parents already has a visibility on the unit,
               -- because the child will inherit this visibility
               --
               Append_To_Container (S);
            else
               Process_Unit (Name    => Ada.Characters.Handling.To_Wide_String (To_Unit (To_String (S.Unit_File_Name))),
                             File    => To_String (S.Unit_File_Name),
                             Project => To_String (S.Parent_GPR_Name));
            end if;
         end loop;
      end Visibility_Analyzis;

      -------------
      -- Add_Additional_Units --
      -------------

      procedure Add_Additional_Units (From : String)
      is
         use Ada.Characters.Handling;
         Pos      : Natural := From'Last + 1;
         U_Pos    : GNAT_GPR.Containers.Cursor;
      begin
         if From /= "" then
            while Pos in Positive loop
               declare
                  Base_File : constant String := Replace (Char => '.',
                                                          To   => "-",
                                                          Item => To_Lower (From (From'First .. Pos - 1))) & ".ad";
               begin
                  U_Pos := All_Sources_Container.Find (Base_File & "b");
                  if GNAT_GPR.Containers.Has_Element (U_Pos) then
                     Append_To_Container (GNAT_GPR.Containers.Element (U_Pos));
                     return;
                  end if;
                  U_Pos := All_Sources_Container.Find (Base_File & "s");
                  if GNAT_GPR.Containers.Has_Element (U_Pos) then
                     Append_To_Container (GNAT_GPR.Containers.Element (U_Pos));
                     return;
                  end if;
               end;
               Pos := Ada.Strings.Fixed.Index (Source  => From,
                                               Pattern => ".",
                                               From    => Pos - 1,
                                               Going   => Ada.Strings.Backward);
            end loop;
         end if;
      end Add_Additional_Units;

      -------------
      -- To_File --
      -------------

      function To_File (U : String) return String
      is
      begin
         return Ada.Characters.Handling.To_Lower (GNAT_GPR.Replace (Char => '.',
                                                                    To   => "-",
                                                                    Item => U));
      end To_File;
      Unit_Under_Analyzis : Unbounded_String;
   begin

      GNAT_GPR.Set_Project_Source_Files (Project               => Project,
                                         Container             => All_Sources_Container,
                                         Recursive             => True,
                                         Exclude_Externally    => Exclude_Externally,
                                         Exclude_Library_Based => False,
                                         Body_Only             => False);


      GNAT_GPR.Set_Project_Source_Files (Project               => Project,
                                         Container             => Sources_Container,
                                         Recursive             => Recursive,
                                         Exclude_Externally    => Exclude_Externally,
                                         Exclude_Library_Based => False,
                                         Body_Only             => False);

      GNAT_GPR.Set_Project_Source_Directories (Project            => Project,
                                               Container          => Directories_Container,
                                               Recursive          => True,
                                               Exclude_Externally => True);

      --  Now we need to add any root units that belongs to another project
      --
      declare
         Missing_Parents : GNAT_GPR.Containers.Map := GNAT_GPR.Containers.Empty_Map;
         Effective_Missing_Parents : GNAT_GPR.Containers.Map := GNAT_GPR.Containers.Empty_Map;
         Missing_Unit    : T_Unit_Information;
         Files           : GNATColl.VFS.File_Array (1 .. Natural (Sources_Container.Length) + 1);
         File_Index      : Positive := 1;
      begin

         Add_Unit_Under_Analyzis:
         declare
            Is_Found : Boolean := False;
         begin
            Level_One :
            for Index in 0 .. Ada.Strings.Fixed.Count (Source  => Unit,
                                                       Pattern => ".") loop
               declare
                  Pattern : constant String := To_File (Extract_Parent (Unit, Index)) & ".ad";
               begin
                  Level_Two:
                  for U of All_Sources_Container loop
                     if Ada.Strings.Fixed.Index (Source  => To_String (U.Unit_File_Name),
                                                 Pattern => Pattern) in Positive then
                        declare
                           Meta   :          T_Unit_Information := U;
                           File   : constant String             := To_String (U.Unit_File_Name);
                           S_File : constant String             := File (File'First .. File'Last - 1) & "s";
                           B_File : constant String             := File (File'First .. File'Last - 1) & "b";
                        begin
                           Meta.Unit_File_Name := To_Unbounded_String (S_File);
                           if All_Sources_Container.Contains (Simple_Name (S_File)) then
                              VF                  := GNATCOLL.VFS.Create (Full_Filename => GNATCOLL.VFS."+" (To_String (Meta.Unit_File_Name)));
                              Files (File_Index)  := VF;
                              File_Index          := File_Index + 1;
                              Is_Found            := True;
                              Unit_Under_Analyzis := To_Unbounded_String (Extract_Parent (Unit, Index));
                           else
                              Meta.Unit_File_Name := To_Unbounded_String (B_File);
                              if All_Sources_Container.Contains (Simple_Name (B_File)) then
                                 VF                  := GNATCOLL.VFS.Create (Full_Filename => GNATCOLL.VFS."+" (To_String (Meta.Unit_File_Name)));
                                 Files (File_Index)  := VF;
                                 File_Index          := File_Index + 1;
                                 Is_Found            := True;
                                 Unit_Under_Analyzis := To_Unbounded_String (Extract_Parent (Unit, Index));
                              end if;
                           end if;
                        end;
                        exit Level_One;
                     end if;
                  end loop Level_Two;
               end;
            end loop Level_One;
            if not Is_Found then
               Ada.Command_Line.Set_Exit_Status (241);
               return;
            end if;
         end Add_Unit_Under_Analyzis;

         Search_For_Missing_Parents:
         for C of Sources_Container loop
            declare
               Unit_File_Name : constant String  := To_String (C.Unit_File_Name);
               Base           : constant String  := Ada.Directories.Base_Name (Unit_File_Name);
               Begin_Pos      : constant Natural := Base'First;
               End_Pos        :          Natural := Base'Last;
            begin
               VF                 := GNATCOLL.VFS.Create (Full_Filename => GNATCOLL.VFS."+" (Unit_File_Name));
               Files (File_Index) := VF;
               File_Index         := File_Index + 1;
               End_Pos := Ada.Strings.Fixed.Index (Source  => Base,
                                                   Pattern => "-",
                                                   From    => Begin_Pos);
               while End_Pos /= 0 loop
                  if (for all U of Sources_Container => Ada.Strings.Fixed.Index (Source  => Ada.Directories.Base_Name (To_String (U.Unit_File_Name)),
                                                                                 Pattern => Base (Begin_Pos .. End_Pos - 1) & ".ad") = 0) then
                     Missing_Unit.Unit_File_Name := To_Unbounded_String (Base (Begin_Pos .. End_Pos - 1) & ".ad");
                     if not Missing_Parents.Contains (Simple_Name (To_String (Missing_Unit.Unit_File_Name))) then
                        Missing_Parents.Include (Simple_Name (To_String (Missing_Unit.Unit_File_Name)),
                                                 Missing_Unit);
                     end if;
                  end if;
                  End_Pos   := Ada.Strings.Fixed.Index (Source  => Base,
                                                        Pattern => "-",
                                                        From    => End_Pos + 1);
               end loop;
            end ;
         end loop Search_For_Missing_Parents;


         Add_Missing_Units:
         for U of All_Sources_Container loop
            for M of Missing_Parents loop
               declare
                  Pattern : constant String := Ada.Directories.Simple_Name (To_String (M.Unit_File_Name));
               begin
                  if Ada.Strings.Fixed.Index (Source  => Ada.Directories.Simple_Name (To_String (U.Unit_File_Name)),
                                              Pattern => Pattern) = 1 then
                     if not Sources_Container.Contains (Simple_Name (To_String (U.Unit_File_Name))) then
                        Sources_Container.Include (Simple_Name (To_String (U.Unit_File_Name)),
                                                   U);
                        Effective_Missing_Parents.Include (Simple_Name (To_String (U.Unit_File_Name)),
                                                           U);
                     end if;
                  end if;
               end;
            end loop;
         end loop Add_Missing_Units;

         Initiate_LAL_Context:
         declare
            Missing_Parent_Files : GNATColl.VFS.File_Array (1 .. Natural (Effective_Missing_Parents.Length));
            File_Index           : Positive := 1;
            use type GNATCOLL.VFS.File_Array;
         begin
            for M of Effective_Missing_Parents loop
               VF                                := GNATCOLL.VFS.Create (Full_Filename => GNATCOLL.VFS."+" (To_String (M.Unit_File_Name)));
               Missing_Parent_Files (File_Index) := VF;
               File_Index                        := File_Index + 1;
            end loop;

            LAL_Auto_Unit_Provider := Libadalang.Auto_Provider.Create_Auto_Provider (Input_Files => Files & Missing_Parent_Files);
            Unit_Provider_Ref      := LAL_Auto_Unit_Provider.Create_Unit_Provider_Reference;
            LAL_Context            := Libadalang.Analysis.Create_Context (Unit_Provider => Unit_Provider_Ref);
         end Initiate_LAL_Context;
      end;

      Update_If_Is_Subunit:
      begin
         for Index in 0 .. Ada.Strings.Fixed.Count (Source  => Unit,
                                                    Pattern => ".") loop
            declare
               U    : constant Libadalang.Analysis.Analysis_Unit'Class := LAL_Auto_Unit_Provider.Get_Unit
                 (Context => LAL_Context,
                  Name    => Langkit_Support.Text.To_Text (Extract_Parent (Unit, Index)),
                  Kind    => Libadalang.Common.Unit_Body);
               Root : constant Libadalang.Analysis.Ada_Node'Class := U.Root;
            begin
               if not U.Has_Diagnostics then
                  if Root.Kind in Libadalang.Common.Ada_Compilation_Unit and then
                    Libadalang.Analysis.F_Body (Libadalang.Analysis.As_Compilation_Unit (Root)).Kind in Libadalang.Common.Ada_Subunit then
                     Unit_Under_Analyzis := To_Unbounded_String (Extract_Parent (Unit, Index + 1));
                 end if;
            end if;
            end;
         end loop;
      end Update_If_Is_Subunit;

      if Sources_Container.Is_Empty then
         Container.Clear;
      else
         Visibility_Analyzis (To_String (Unit_Under_Analyzis));

         if Include_Unit_Parents then
            -- we will add all the parents of the unit
            --
            Add_Additional_Units (Unit);
         end if;

         Add_Additional_Units (Additional_Unit);

         if Get_Asis_Tree_Files then
            --  we will output ASIS tree files instead of source files
            --
            declare
               Asis_Files : Containers.Map;
            begin
               Compute_Asis_Tree_Files (Units     => Container,
                                        Root_Dir  => Root_Dir,
                                        Relocate  => Relocate,
                                        Container => Asis_Files);
               --  ASIS tree files is computed on the units that have visibility
               --  on the reference unit
               --

               Container := Asis_Files;
            end;
         end if;
      end if;
   end Set_Project_Source_Files_That_Have_Visibility_On;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_GPRs  (Project            : in     T_Project;
                                Container          : in out Containers.Map;
                                Exclude_Externally : in     Boolean;
                                Recursive          : in     Boolean)
   is
      pragma Unreferenced (Project);
      use GNATCOLL.Projects;
      Iterator     :          Project_Iterator := Start (Tree.Root_Project,
                                                         Direct_Only => not Recursive);
      Project_Info :          Project_Type;
      Unit         :          T_Unit_Information;
      Root         : constant String := GNATColl.Projects.Internals.File_Name (Tree.Root_Project);
   begin
      loop
         Project_Info := Current (Iterator);
         exit when Project_Info = GNATCOLL.Projects.No_Project;

         Unit.Parent_GPR_Name := To_Unbounded_String (GNATColl.Projects.Internals.File_Name (Project_Info));
         if not Container.Contains (Simple_Name (To_String (Unit.Parent_GPR_Name))) then
            if not Exclude_Externally or else
              (Exclude_Externally and then not GNATColl.Projects.Internals.Externally_Built (Project_Info)) then
               if Unit.Parent_GPR_Name /= Root then
                  --  root project shall not be listed in the projects that belongs to itself
                  --  this adresses aggregated project cases
                  --
                  Container.Include (Simple_Name (To_String (Unit.Parent_GPR_Name)),
                                     Unit);
               end if;
            end if;
         end if;
         Unit.Parent_GPR_Name := Null_Unbounded_String;
         Next (Iterator);
      end loop;
   end Set_Project_GPRs;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_Source_Directories (Project            : in     T_Project;
                                             Container          : in out Containers.Map;
                                             Recursive          : in     Boolean;
                                             Exclude_Externally : in     Boolean)
   is
      pragma Unreferenced (Exclude_Externally);
      use GNATCOLL.Projects,
          GNATCOLL.VFS;
      Unit : T_Unit_Information;
   begin
      Container.Clear;

      for Dir of Source_Dirs (Project   => Project.Handler,
                              Recursive => Recursive) loop
         Unit.Unit_File_Name  := To_Unbounded_String (+Full_Name (Dir));
         Container.Include (Simple_Name (To_String (Unit.Unit_File_Name)),
                            Unit);
      end loop;

   end Set_Project_Source_Directories;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   procedure Set_Project_Mains (Project             : in     T_Project;
                                Transitive_Analysis : in     Boolean;
                                Container           : in out Containers.Map)
   is
      use GNATCOLL.Projects,
          GNATCOLL.VFS;
      Iterator     : Project_Iterator := Start (Project.Handler,
                                                Recursive   => Transitive_Analysis,
                                                Direct_Only => not Transitive_Analysis);
      Project_Info : Project_Type;
      Unit         : T_Unit_Information;
   begin

      loop
         Project_Info := Current (Iterator);
         exit when Project_Info = GNATCOLL.Projects.No_Project;

         for Dir of File_And_Project_Array_Access'(Source_Files (Project   => Project_Info,
                                                                 Recursive => False)).all loop

            if Is_Main_File (Project        => Project_Info,
                             File           => Base_Name (Dir.File),
                             Case_Sensitive => False) then
               Unit.Unit_File_Name  := To_Unbounded_String (+Full_Name ((Dir.File)));
               Unit.Parent_GPR_Name := To_Unbounded_String (+Full_Name (Dir.Project.Project_Path));

               Container.Include (Simple_Name (To_String (Unit.Unit_File_Name)),
                                  Unit);
            end if;
         end loop;
         Next (Iterator);
      end loop;
   end Set_Project_Mains;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Replace (Char : Character;
                     To   : String;
                     Item : String) return String
   is
      Result : Unbounded_String := Null_Unbounded_String;
   begin
      for I in Item'Range loop
         if Item (I) = Char then
            Append (Result, To);
         else
            Append (Result, Item (I));
         end if;
      end loop;
      return To_String (Result);
   end Replace;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Replace (From : String;
                     To   : String;
                     Item : String) return String
   is
      Pos : Natural := 0;
   begin
      if From = "" then
         return Item;
      else
         Pos := Ada.Strings.Fixed.Index (Source  => Item,
                                         Pattern => From);
         if Pos = 0 then
            return Item;
         else
            if Pos > Item'First then
               return Item (Item'First .. Pos - 1) & To & Replace (From => From,
                                                                   To   => To,
                                                                   Item => Item (Pos + From'Length .. Item'Last));
            else
               return To & Replace (From => From,
                                    To   => To,
                                    Item => Item (Pos + From'Length .. Item'Last));
            end if;
         end if;
      end if;
   end Replace;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Is_Ads (File : String) return Boolean
   is
   begin
      return File (File'Last - 3 .. File'Last) = ".ads" or else
      File (File'Last - 3 .. File'Last) = ".ADS";
   end Is_Ads;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function Is_Adb (File : String) return Boolean
   is
   begin
      return File (File'Last - 3 .. File'Last) = ".adb" or else
      File (File'Last - 3 .. File'Last) = ".ADB";
   end Is_Adb;

   -- --------------------------------------------------------------------------
   -- Extract_Parent
   -- --------------------------------------------------------------------------

   function Extract_Parent (Unit  : String;
                            Level : Natural) return String
   --  When Level equals zero it return the unit itself
   --
   is
      Pos : Natural := Unit'Last + 1;
   begin
      for Index in 1 .. Level loop
         Pos := Ada.Strings.Fixed.Index (Source  => Unit,
                                         Pattern => ".",
                                         Going   => Ada.Strings.Backward,
                                         From    => Pos - 1);
      end loop;
      return Unit (Unit'First .. Pos - 1);
   end Extract_Parent;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function To_Mixed (S : String) return String
   is
      R            : String := S;
      Upper_Needed : Boolean := True;
   begin
      for C in R'Range loop
         if Upper_Needed then
            R(C) := Ada.Characters.Handling.To_Upper (R(C));
         else
            R(C) := Ada.Characters.Handling.To_Lower (R(C));
         end if;

         if R(C) = '.' or R(C) = '_' then
            Upper_Needed := True;
         else
            Upper_Needed := False;
         end if;
      end loop;
      return R;
   end To_Mixed;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------

   function To_Unit (S : String) return String
   is
      Simple : constant String := Ada.Directories.Simple_Name (S);
   begin
      if Simple'Length >= 4 and then Simple (Simple'Last - 3 .. Simple'Last) in ".adb" | ".ads" | ".adt" then
         return To_Mixed (GNAT_GPR.Replace (Char => '-',
                                            To   => ".",
                                            Item => Simple (Simple'First .. Simple'Last - 4)));
      else
         return To_Mixed (GNAT_GPR.Replace (Char => '-',
                                            To   => ".",
                                            Item => Simple));
      end if;
   end To_Unit;

end GNAT_GPR;
