--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2020, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

with GPR.Env,
     GPR.Names;

use GPR,
    GPR.Names;

with GNATCOLL.VFS;

use GNATCOLL.VFS;

package body GNATColl.Projects.Internals is

   ------------------------------------------------------------------------------------------------------
   --
   ------------------------------------------------------------------------------------------------------

   ---------------
   -- Is_Library_Based --
   ---------------

   function Is_Library_Based (Self : Project_Type) return Boolean is
   begin
      return Get_View (Self).Library;
   end Is_Library_Based;

   ---------------
   -- Source_Files --
   ---------------

   function Source_Files (Project               : Project_Type;
                          Recursive             : Boolean := False;
                          Include_Project_Files : Boolean := False;
                          Exclude_Externally    : Boolean := False;
                          Exclude_Library_Based : Boolean := False)
                          return File_And_Project_Array_Access
   is
      Count   : Natural := 0;
      Index   : Natural;
      P       : Project_Type;
      Result  : File_And_Project_Array_Access;
      Iter    : Project_Iterator := Start (Project, Recursive => Recursive);
   begin
      --  Count files

      loop
         P := Current (Iter);
         exit when P = No_Project;

         if (not Exclude_Externally or else (Exclude_Externally and then not Get_View (P).Externally_Built))
           and then
             (not Exclude_Library_Based or else (Exclude_Library_Based and then not Is_Library_Based (P)))
         then
            if P.Data.Files /= null then
               Count := Count + P.Data.Files'Length;
            end if;

            if Include_Project_Files then
               Count := Count + 1;
            end if;
         end if;

         Next (Iter);
      end loop;

      Result := new File_And_Project_Array (1 .. Count);
      Index := Result'First;

      Iter := Start (Project, Recursive => Recursive);

      loop
         P := Current (Iter);
         exit when P = No_Project;

         if (not Exclude_Externally or else (Exclude_Externally and then not Get_View (P).Externally_Built))
          and then
             (not Exclude_Library_Based or else (Exclude_Library_Based and then not Is_Library_Based (P)))
         then

            if Include_Project_Files then
               Result (Index) :=
                 (File    => P.Project_Path,
                  Project => P);
               Index := Index + 1;
            end if;

            if P.Data.Files /= null then
               for S in P.Data.Files'Range loop
                  Result (Index) :=
                    (File    => P.Data.Files (S),
                     Project => P);
                  Index := Index + 1;
               end loop;
            end if;
         end if;
         Next (Iter);
      end loop;

      return Result;
   end Source_Files;

   ---------------
   -- Externally_Built --
   ---------------

   function Externally_Built (Project : Project_Type'Class) return Boolean
   is
   begin
      return Get_View (Project).Externally_Built;
   end Externally_Built;


   ---------------
   -- File_Name --
   ---------------

   function File_Name (Project : Project_Type'Class) return String is
   begin
      return GPR.Names.Get_Name_String (Project.Data.View.Path.Name);
   end File_Name;

   ----------------
   -- Initialize --
   ----------------

   procedure Initialize (Error_Handler : GPR.Error_Handler;
                         Self          : out Project_Environment_Access)
   is
      Flags : constant Processing_Flags := GPR.Create_Flags (Report_Error               => Error_Handler,
                                                             When_No_Sources            => Error,
                                                             Require_Sources_Other_Lang => False,
                                                             Allow_Duplicate_Basenames  => True,
                                                             Compiler_Driver_Mandatory  => False,
                                                             Error_On_Unknown_Language  => True,
                                                             Require_Obj_Dirs           => Silent,
                                                             Allow_Invalid_External     => Silent,
                                                             Missing_Source_Files       => Silent,
                                                             Ignore_Missing_With        => True,
                                                             Check_Configuration_Only   => False);
   begin
      if Self = null then
         Self := new Project_Environment;

         GPR.Tree.Initialize (Self.Env,
                              Flags => Flags);

         GPR.Env.Initialize_Default_Project_Path
           (Self.Env.Project_Path, Target_Name => "");
      end if;
   end Initialize;

end GNATColl.Projects.Internals;
