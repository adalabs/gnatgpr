--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2018, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

private with
GNATCOLL.Projects;

-- Ada
with Ada.Strings.Unbounded,
     Ada.Containers.Indefinite_Ordered_Maps,
     Ada.Containers.Vectors;

use  Ada.Strings.Unbounded;

package GNAT_GPR is

   type T_Project_Kinds is (Both,
                            Externally_Built,
                            Internally_Built);

   type T_Directory_Information is record
      GPR_Name       : Unbounded_String := Null_Unbounded_String;
      Directory_Name : Unbounded_String := Null_Unbounded_String;
      CSC_Name       : Unbounded_String := Null_Unbounded_String;
   end record;

   -- --------------------------------------------------------------------------

   function Equal (Left  : T_Directory_Information;
                   Rigth : T_Directory_Information)
                   return Boolean;


   -- --------------------------------------------------------------------------
   package Directory_Info_Container is new Ada.Containers.Vectors (Index_Type   => Positive,
                                                                   Element_Type => T_Directory_Information,
                                                                   "="          => Equal);

   Directory_Information_Container : Directory_Info_Container.Vector;

   type Unit_Kind_Type is (Not_Applicable,
                           A_Unit,
                           A_Sub_Unit);

   type T_Unit_Information is record
      Parent_GPR_Name : Unbounded_String := Null_Unbounded_String;
      Unit_File_Name  : Unbounded_String := Null_Unbounded_String;
   end record;

   K_Null_Unit_Information : constant T_Unit_Information := (Null_Unbounded_String, Null_Unbounded_String);

   -- --------------------------------------------------------------------------

   function Equals (Left  : T_Unit_Information;
                    Right : T_Unit_Information)
                    return Boolean;

   function Is_Less (L, R : T_Unit_Information) return Boolean;

   -- --------------------------------------------------------------------------

   package Vector_Containers is new Ada.Containers.Vectors (Index_Type   => Positive,
                                                            Element_Type => T_Unit_Information,
                                                            "="          => Equals);

   package Containers is new Ada.Containers.Indefinite_Ordered_Maps (Key_Type     => String, -- Simple_Name
                                                                     Element_Type => T_Unit_Information,
                                                                     "<"          => "<",
                                                                     "="          => Equals);

   type T_Project is private;

   type T_File_Information is (Relative, Absolute);


   -- --------------------------------------------------------------------------

   procedure Initialize;

   -- --------------------------------------------------------------------------

   procedure Finalize;


   -- --------------------------------------------------------------------------

   procedure Load_GPR (Name    : in     String;
                       Project :    out T_Project);

   -- ----------
   -- Get *
   -- ----------

   -- --------------------------------------------------------------------------

   function Get_Project_Source_Paths (Project            : in T_Project;
                                      Exclude_Externally : in Boolean;
                                      Prepend            : in String;
                                      PostPend           : in String) return String;

   -- --------------------------------------------------------------------------

   function Get_Project_Object_Paths (Project             : in T_Project;
                                      Including_Libraries : in Boolean;
                                      Recursive           : in Boolean;
                                      Exclude_Externally  : in Boolean;
                                      Prepend             : in String;
                                      Postpend            : in String) return String;

   -- --------------------------------------------------------------------------

   function Get_Project_Executable_Path (Project : in T_Project) return String;

   -- ----------
   -- Set *
   -- ----------

   -- --------------------------------------------------------------------------

   procedure Set_Project_Object_Paths (Project             : in     T_Project;
                                       Objects_Paths       :    out Directory_Info_Container.Vector;
                                       Recursive           : in     Boolean;
                                       Including_Libraries : in     Boolean;
                                       Exclude_Externally  : in     Boolean);

   -- --------------------------------------------------------------------------

   procedure Set_Project_GPRs  (Project            : in     T_Project;
                                Container          : in out Containers.Map;
                                Exclude_Externally : in     Boolean;
                                Recursive          : in     Boolean);

   -- --------------------------------------------------------------------------

   procedure Set_Project_Source_Files (Project               : in     T_Project;
                                       Container             : in out Containers.Map;
                                       Recursive             : in     Boolean;
                                       Exclude_Externally    : in     Boolean;
                                       Exclude_Library_Based : in     Boolean;
                                       Body_Only             : in     Boolean := False);

   -- --------------------------------------------------------------------------

   procedure Set_Project_Source_Files_That_Have_Visibility_On (Unit                           : in     String;
                                                               Project                        : in     T_Project;
                                                               Recursive                      : in     Boolean;
                                                               Exclude_Externally             : in     Boolean;
                                                               Get_Asis_Tree_Files            : in     Boolean;
                                                               Include_Unit_Parents           : in     Boolean;
                                                               Additional_Unit                : in     String;
                                                               Container                      : in out Containers.Map);

   -- --------------------------------------------------------------------------

   procedure Set_Project_Source_Directories (Project            : in     T_Project;
                                             Container          : in out Containers.Map;
                                             Recursive          : in     Boolean;
                                             Exclude_Externally : in     Boolean);
   --  Exclude_Externally can not be managed for directories as GNATColl.Source_Dirs
   --  use a lot of data structures that are internal in Projects body (i.e Project_Tree_Data)
   --



   -- --------------------------------------------------------------------------

   procedure Set_Project_Mains (Project             : in     T_Project;
                                Transitive_Analysis : in     Boolean;
                                Container           : in out Containers.Map);

   -- --------------------------------------------------------------------------

   procedure Set_Project_Executables (Project            : in     T_Project;
                                      Container          :    out Containers.Map;
                                      Including_Others   : in     Boolean);

   -- --------------------------------------------------------------------------

   procedure Set_Project_Source_File_Zoombies (Project               : in     String;
                                               Container             :    out Containers.Map;
                                               Transitive_Analysis   : in     Boolean;
                                               Exclude_Externally    : in     Boolean;
                                               Exclude_Library_Based : in     Boolean);

   -- --------------------------------------------------------------------------

   procedure Set_Project_Object_File_Zoombies (Project             : in     String;
                                               Container           :    out Containers.Map;
                                               Transitive_Analysis : in     Boolean;
                                               Exclude_Externally  : in     Boolean);
   -- ----------
   -- Display *
   -- ----------


   -- --------------------------------------------------------------------------

   procedure Display_Project_Sources (Container                  : in Containers.Map;
                                      With_Corresponding_Project : in Boolean;
                                      With_Unit_Kind             : in Boolean);

   -- --------------------------------------------------------------------------

   procedure Display_Project_Source_Directories (Container : in Containers.Map);

   -- --------------------------------------------------------------------------

   procedure Display_Project_GPR (Container : in Containers.Map);

   -- --------------------------------------------------------------------------

   procedure Display_Project_Object_Paths (Project             : in T_Project;
                                           Recursive           : in Boolean;
                                           Including_Libraries : in Boolean;
                                           Exclude_Externally  : in Boolean);

   -- --------------------------------------------------------------------------

   procedure Display_Project_Executable_Path  (Project          : in T_Project);

   -- --------------------------------------------------------------------------

   procedure Display_Project_Executables (Project            : in T_Project;
                                          Including_Others   : in Boolean);
   -- --------------------------------------------------------------------------

   procedure Display_Project_Object_File_Zoombies (Project             : in String;
                                                   Transitive_Analysis : in Boolean;
                                                   Exclude_Externally  : in Boolean);

   -- ----------
   -- Utilities
   -- ----------

   -- --------------------------------------------------------------------------

   function Is_Ads (File : String) return Boolean;

   -- --------------------------------------------------------------------------

   function Is_Adb (File : String) return Boolean;


   -- --------------------------------------------------------------------------

   function Replace (Char : Character;
                     To   : String;
                     Item : String) return String;

   -- --------------------------------------------------------------------------
   function Replace (From : String;
                     To   : String;
                     Item : String) return String;

   -- --------------------------------------------------------------------------

   function To_Mixed (S : String) return String;

   -- --------------------------------------------------------------------------

   function To_Unit (S : String) return String;

   -- --------------------------------------------------------------------------

   function Extract_Parent (Unit  : String;
                            Level : Natural) return String;

private

   type T_Project is record
      Handler : GNATCOLL.Projects.Project_Type;
   end record;

   No_Project       : constant T_Project := (Handler => GNATCOLL.Projects.No_Project);
   Init_Output_Done :          Boolean   := False;
   pragma Atomic (Init_Output_Done);
end GNAT_GPR;
