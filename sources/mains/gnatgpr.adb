------------------------------------------------------------------------------
--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2021, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

-- Ada

with Ada.Exceptions,
     Ada.Characters.Latin_1,
     Ada.Text_IO,
     Ada.Strings.Unbounded;

use  Ada.Strings.Unbounded;

-- GNAT
with GNAT.OS_Lib;

-- GNATGPR
with
--GNAT_GPR,
     GNAT_GPR.Options,
     GNAT_GPR.Versions;

pragma Elaborate_All (GNAT_GPR);

procedure GNATGPR is
   New_Line                       : constant String := "" & Ada.Characters.Latin_1.LF;
   Project                        :          GNAT_GPR.T_Project;
   Sources_Container              :          GNAT_GPR.Containers.Map := GNAT_GPR.Containers.Empty_Map;
   -- --------------------------------------------------------------------------
   procedure Initialize (Root_GPR : in String) is
   begin
      GNAT_GPR.Load_GPR (Name    => Root_GPR,
                         Project => Project);
   end Initialize;

   -- --------------------------------------------------------------------------
   procedure Finalize is
   begin
      GNAT_GPR.Finalize;
   end Finalize;

   use GNAT_GPR.Options;
begin
   Analyse_Options;

   GNAT_GPR.Initialize;

   case Action is
      when Help =>

         Help_Options;
         User_Message ("Copyright (C) 2007-2021, AdaLabs Ltd.");
         User_Message ("This software is covered by the GNU General Public License.");
         User_Message ("https://adalabs.com");

      when Version =>

         User_Message ("gnatgpr version : " & GNAT_GPR.Versions.Value);

      when List_Binaries =>

         Initialize (Root_GPR => To_String (Action_Value (List_Binaries)));
         GNAT_GPR.Display_Project_Executables (Project,
                                               Transitive_Analysis);
         Finalize;

      when List_Binaries_Directories =>

         Initialize (Root_GPR => To_String (Action_Value (List_Binaries_Directories)));
         GNAT_GPR.Display_Project_Executable_Path (Project);
         Finalize;

      when List_Sources =>

         Initialize (Root_GPR => To_String (Action_Value (List_Sources)));

         GNAT_GPR.Set_Project_Source_Files (Project               => Project,
                                            Container             => Sources_Container,
                                            Recursive             => Transitive_Analysis,
                                            Exclude_Externally    => Exclude_Externally,
                                            Exclude_Library_Based => Exclude_Library_Based);
         GNAT_GPR.Display_Project_Sources (Container                  => Sources_Container,
                                           With_Corresponding_Project => False,
                                           With_Unit_Kind             => False);
         Finalize;

      when List_Sources_That_Have_Visibility_On =>

         Initialize (Root_GPR => To_String (Action_Value (List_Sources_That_Have_Visibility_On)));

         GNAT_GPR.Set_Project_Source_Files_That_Have_Visibility_On (Unit                           => To_String (With_Entity),
                                                                    Project                        => Project,
                                                                    Recursive                      => GNAT_GPR.Options.Transitive_Analysis,
                                                                    Exclude_Externally             => GNAT_GPR.Options.Exclude_Externally,
                                                                    Get_Asis_Tree_Files            => GNAT_GPR.Options.Asis_Tree_Files,
                                                                    Include_Unit_Parents           => False,
                                                                    Container                      => Sources_Container,
                                                                    Additional_Unit                => To_String (GNAT_GPR.Options.Additional_Unit));

         GNAT_GPR.Display_Project_Sources (Container                  => Sources_Container,
                                           With_Corresponding_Project => True,
                                           With_Unit_Kind             => True);
         Finalize;

      when List_Source_Directories =>

         Initialize (Root_GPR => To_String (Action_Value (List_Source_Directories)));
         GNAT_GPR.Set_Project_Source_Directories (Project            => Project,
                                                  Container          => Sources_Container,
                                                  Recursive          => Transitive_Analysis,
                                                  Exclude_Externally => Exclude_Externally);

         GNAT_GPR.Display_Project_Source_Directories (Container => Sources_Container);
         Finalize;

      when List_Sources_And_Projects =>

         Initialize (Root_GPR => To_String (Action_Value (List_Sources_And_Projects)));
         GNAT_GPR.Set_Project_Source_Files (Project               => Project,
                                            Container             => Sources_Container,
                                            Recursive             => Transitive_Analysis,
                                            Exclude_Externally    => Exclude_Externally,
                                            Exclude_Library_Based => Exclude_Library_Based);

         GNAT_GPR.Display_Project_Sources (Container                  => Sources_Container,
                                           With_Corresponding_Project => True,
                                           With_Unit_Kind             => False);
         Finalize;

      when List_Mains =>

         Initialize (Root_GPR => To_String (Action_Value (List_Mains)));
         GNAT_GPR.Set_Project_Mains       (Project             => Project,
                                           Transitive_Analysis => Transitive_Analysis,
                                           Container           => Sources_Container);

         GNAT_GPR.Display_Project_Sources (Container                  => Sources_Container,
                                           With_Corresponding_Project => False,
                                           With_Unit_Kind             => False);
         Finalize;

      when List_Projects =>

         Initialize (Root_GPR => To_String (Action_Value (List_Projects)));

         GNAT_GPR.Set_Project_GPRs (Project            => Project,
                                    Container          => Sources_Container,
                                    Exclude_Externally => Exclude_Externally,
                                    Recursive          => Transitive_Analysis);

         GNAT_GPR.Display_Project_GPR (Container       => Sources_Container);

         Finalize;

      when List_Object_Paths =>

         Initialize (Root_GPR => To_String (Action_Value (List_Object_Paths)));
         GNAT_GPR.Display_Project_Object_Paths (Project,
                                                Recursive           => Transitive_Analysis,
                                                Including_Libraries => Transitive_Analysis,
                                                Exclude_Externally  => Exclude_Externally);
         Finalize;

      when List_Object_Zombies =>

         GNAT_GPR.Display_Project_Object_File_Zoombies (Project             => To_String (Action_Value (List_Object_Zombies)),
                                                        Transitive_Analysis => Transitive_Analysis,
                                                        Exclude_Externally  => Exclude_Externally);
         Finalize;

      when List_Source_Zombies =>
         declare
            Results : GNAT_GPR.Containers.Map;
         begin
            GNAT_GPR.Set_Project_Source_File_Zoombies (Project               => To_String (Action_Value (List_Source_Zombies)),
                                                       Container             => Results,
                                                       Transitive_Analysis   => Transitive_Analysis,
                                                       Exclude_Externally    => Exclude_Externally,
                                                       Exclude_Library_Based => Exclude_Library_Based);
            for Z of Results loop
               Ada.Text_IO.Put_Line (To_String (Z.Unit_File_Name));
            end loop;
         end;

         Finalize;
   end case;

   if Use_Output_File then
      Ada.Text_IO.Close (File => Output_File);
   end if;

exception
   when E : others =>
      User_Message ("Unexpected error " &
                      Ada.Exceptions.Exception_Name (E) &
                      New_Line &
                      Ada.Exceptions.Exception_Information (E));
      GNAT.OS_Lib.OS_Exit (1);
end GNATGPR;
