------------------------------------------------------------------------------
--                                                                          --
--                         GNATGPR                                          --
--                                                                          --
--                                                                          --
--          Copyright (C) 2007-2011, AdaLabs Ltd.                           --
--                                                                          --
--                      http://adalabs.com                                  --
--                                                                          --
-- GNATGPR is free software; you can redistribute it and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 2,  or (at your option) any later ver- --
-- sion.  GNAT is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License --
-- for  more details.  You should have  received  a copy of the GNU General --
-- Public License  distributed with GNAT;  see file COPYING.  If not, write --
-- to  the  Free Software Foundation,  51  Franklin  Street,  Fifth  Floor, --
-- Boston, MA 02110-1301, USA.                                              --
--                                                                          --
------------------------------------------------------------------------------
-- Ada
with Ada.Text_IO,
     Ada.Strings.Unbounded;

use  Ada.Strings.Unbounded;

-- GNAT
with GNAT.String_Split;

package body GNAT_GPR is
   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------
   function Replace (Char : Character;
                     To   : String;
                     Item : String) return String is
      Source : constant Unbounded_String := To_Unbounded_String (Item);
      Result : Unbounded_String          := Null_Unbounded_String;
   begin
      for I in Item'Range loop
         if Item (I) = Char then
            Append (Result, To);
         else
            Append (Result,To_String (Unbounded_Slice
              (Source => Source,
               Low    => I,
               High   => I)));
         end if;
      end loop;
      --Ada.Text_IO.Put_Line (To_String (Result));
      return To_String (Result);
   end;

end GNAT_GPR;
