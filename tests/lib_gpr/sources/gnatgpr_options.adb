------------------------------------------------------------------------------
--                                                                          --
--                         GNATGPR                                          --
--                                                                          --
--                                                                          --
--          Copyright (C) 2007-2011, AdaLabs Ltd.                           --
--                                                                          --
--                      http://adalabs.com                                  --
--                                                                          --
-- GNATGPR is free software; you can redistribute it and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 2,  or (at your option) any later ver- --
-- sion.  GNAT is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License --
-- for  more details.  You should have  received  a copy of the GNU General --
-- Public License  distributed with GNAT;  see file COPYING.  If not, write --
-- to  the  Free Software Foundation,  51  Franklin  Street,  Fifth  Floor, --
-- Boston, MA 02110-1301, USA.                                              --
--                                                                          --
------------------------------------------------------------------------------
-- Ada
with Ada.Characters.Handling,
     Ada.Text_IO,
     Ada.Exceptions;

-- GNATGPR
with Options_Analyzer;

package body GNATGPR_Options is

   procedure User_Log     (Message : String);
   procedure Option_Error (Message : String);
   procedure Option_Error (Occur : Ada.Exceptions.Exception_Occurrence);


   -- --------------------------------------------------------------------------
   package Analyzer is
     new Options_Analyzer (Binary_Options => "ohpsmv",
                           Valued_Options => "ohpsmv",
                           Tail_Separator => "--");


   ------------------
   -- Help_Options --
   ------------------
   -- Todo options : -r recursive
   procedure Help_Options is
   begin
      User_Message ("Usage: ");
      User_Message ("       gnatgpr -p <project file>");
      User_Message ("                  Display all included projects");
      User_Message ("       gnatgpr -s <project file>");
      User_Message ("                  Display all included sources");
      User_Message ("       gnatgpr -m <project file>");
      User_Message ("                  Display all main files");
      User_Message ("       gnatgpr -o <project file>");
      User_Message ("                  Display all included object paths");
      User_Message ("       gnatgpr -h ");
      User_Message ("                  Display help");
      User_Message ("       gnatgpr -v ");
      User_Message ("                  Display version");
   end Help_Options;


   ---------------------
   -- Analyse_Options --
   ---------------------
   procedure Analyse_Options is
      use Ada.Characters.Handling;
      use Analyzer;
   begin
      --
      -- Help
      --
      if Is_Present (Option => 'h') then
         Action := Help;
      elsif Is_Present (Option => 'v') then
         Action := Version;
      elsif Is_Present (Option => 'p') and Parameter_Count > 0 then
         Action                       := List_Projects;
         Action_Value (List_Projects) := To_Unbounded_String
           (Source          => Parameter (1));
      elsif Is_Present (Option => 's') and Parameter_Count > 0 then
         Action                      := List_Sources;
         Action_Value (List_Sources) := To_Unbounded_String
           (Source => Parameter (1));
      elsif Is_Present (Option => 'm') and Parameter_Count > 0 then
         Action                    := List_Mains;
         Action_Value (List_Mains) := To_Unbounded_String
           (Source => Parameter (1));
      elsif Is_Present (Option => 'o') and Parameter_Count > 0 then
         Action                           := List_Object_Paths;
         Action_Value (List_Object_Paths) := To_Unbounded_String
           (Source => Parameter (1));
      end if;

   exception
      when Occur : Analyzer.Options_Error =>
         Help_Options;
         Option_Error (Occur);
   end Analyse_Options;

   procedure User_Message (Message : String) is
   begin
      Ada.Text_IO.Put_Line (Message);
   end User_Message;

   procedure User_Log     (Message : String) is
   begin
      Ada.Text_IO.Put_Line (Message);
   end User_Log;


   ------------------
   -- Option_Error --
   ------------------

   procedure Option_Error (Message : String) is
      use Ada.Exceptions;
   begin
      Raise_Exception (Options_Error'Identity, Message);
   end Option_Error;

   procedure Option_Error (Occur : Ada.Exceptions.Exception_Occurrence) is
      use Ada.Exceptions;
   begin
      Option_Error (Exception_Message (Occur));
   end Option_Error;

end GNATGPR_Options;
