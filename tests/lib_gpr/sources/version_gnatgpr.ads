------------------------------------------------------------------------------
 --                                                                          --
 --                         GNATGPR                                          --
 --                                                                          --
 --                                                                          --
 --          Copyright (C) 2007-2011, AdaLabs Ltd.                           --
 --                                                                          --
 --                      http://adalabs.com                                  --
 --                                                                          --
 -- GNATGPR is free software; you can redistribute it and/or modify it under --
 -- terms of the  GNU General Public License as published  by the Free Soft- --
 -- ware  Foundation;  either version 2,  or (at your option) any later ver- --
 -- sion.  GNAT is distributed in the hope that it will be useful, but WITH- --
 -- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
 -- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License --
 -- for  more details.  You should have  received  a copy of the GNU General --
 -- Public License  distributed with GNAT;  see file COPYING.  If not, write --
 -- to  the  Free Software Foundation,  51  Franklin  Street,  Fifth  Floor, --
 -- Boston, MA 02110-1301, USA.                                              --
 --                                                                          --
 ------------------------------------------------------------------------------
 -- This is a generated file
 package Version_GNATGPR is
   -- 20080314-2042               
   K_Version : constant String := "0.4";
 end Version_GNATGPR;

