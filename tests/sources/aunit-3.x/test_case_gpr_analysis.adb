--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2018, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

-- Ada
with --  Ada.Text_IO,
     --  Ada.Exceptions,
     Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

-- GNAT
with GNAT.String_Split,
     GNAT.OS_Lib;

-- AUnit
with AUnit.Assertions;

-- GNATGPR
with GNAT_GPR;

with Tester;
with Ada.Text_IO;
with Ada.Directories;

package body Test_Case_GPR_Analysis is
   Project   : GNAT_GPR.T_Project;
   Container : GNAT_GPR.Containers.Map := GNAT_GPR.Containers.Empty_Map;


   function TS (Source : String) return Unbounded_String renames To_Unbounded_String;


   procedure Test_GPR_Analysis
     (T : in out Aunit.Test_Cases.Test_Case'Class)
   is
      pragma Unreferenced (T);
   begin
      GNAT_GPR.Finalize;
      GNAT_GPR.Initialize;
      GNAT_GPR.Load_GPR (Name    => "../basic_gpr/basicgpr.gpr",
                         Project => Project);
      ---------------
      -- Project Mains
      ---------------
      GNAT_GPR.Set_Project_Mains (Project, Transitive_Analysis => False, Container => Container);
      Tester.Assert_Container
        ("Project_Mains",
         Container,
         (
            (TS ("basicgpr.gpr"),
          TS ("gnatgpr.adb")),
          others => GNAT_GPR.K_Null_Unit_Information));

      GNAT_GPR.Display_Project_Sources (Container,
                                        With_Corresponding_Project => False,
                                        With_Unit_Kind             => False);

      ---------------
      -- Project Sources
      ---------------
      GNAT_GPR.Set_Project_Source_Files (Project               => Project,
                                         Container             => Container,
                                         Recursive             => True,
                                         Exclude_Externally    => True,
                                         Exclude_Library_Based => False);
      Tester.Assert_Container
        ("Project_Sources",
         Container,
         (
            (TS ("basicgpr.gpr"),
          TS ("version_gnatgpr.ads")),
          (TS ("basicgpr.gpr"),
           TS ("gnatgpr.adb")),
          (TS ("basicgpr.gpr"),
           TS ("gnat_gpr.adb")),
          (TS ("basicgpr.gpr"),
           TS ("gnatgpr_options.ads")),
          (TS ("basicgpr.gpr"),
           TS ("gnatgpr_options.adb")),
          (TS ("basicgpr.gpr"),
           TS ("gnat_gpr.ads")),
          (TS ("basicgpr.gpr"),
           TS ("options_analyzer.adb")),
          (TS ("basicgpr.gpr"),
           TS ("options_analyzer.ads")),
          others => GNAT_GPR.K_Null_Unit_Information));

      GNAT_GPR.Display_Project_Sources (Container, False, False);

      declare
         Result : constant String
           := GNAT_GPR.Get_Project_Source_Paths (Project            => Project,
                                                 Prepend            => "[Prepend]",
                                                 PostPend           => "[Postpend] ",
                                                 Exclude_Externally => True);
      begin
         AUnit.Assertions.Assert (Result /= "",
                                  "Error on Get_Project_Source_Paths");
      end;
   end Test_GPR_Analysis;


   procedure Test_Lib_GPR_Analysis
     (T : in out Aunit.Test_Cases.Test_Case'Class)
   is
      pragma Unreferenced (T);
      Obtained : Tester.String_Containers.Vector;
      Expected : Tester.String_Containers.Vector;
   begin
      GNAT_GPR.Finalize;
      GNAT_GPR.Initialize;

      GNAT_GPR.Load_GPR (Name    => "../lib_gpr/libgpr.gpr",
                         Project => Project);
      ---------------
      -- Project Object path
      ---------------
      declare
         Slice_Set            : GNAT.String_Split.Slice_Set;
         Slice_Number         : GNAT.String_Split.Slice_Number
           := GNAT.String_Split.Slice_Number'First;
         Project_Object_Paths : constant String := GNAT_GPR.Get_Project_Object_Paths (Project,
                                                                                      True,
                                                                                      True,
                                                                                      False,
                                                                                      "",
                                                                                      GNAT.OS_Lib.Path_Separator & "");
         use type GNAT.String_Split.Slice_Number;
      begin
         GNAT.String_Split.Create
           (Slice_Set,
            Project_Object_Paths,
            GNAT.OS_Lib.Path_Separator & "");
         Slice_Number := GNAT.String_Split.Slice_Count (Slice_Set);
         if Slice_Number >= GNAT.String_Split.Slice_Number'First + 1 then
            for Index in GNAT.String_Split.Slice_Number'First + 1 .. Slice_Number - 1 loop
               Obtained.Append (TS (GNAT.String_Split.Slice
                 (Slice_Set, Index)));
            end loop;
         end if;
      end;

      Expected.Append (TS ("libraries"));
      Expected.Append (TS ("objects"));

      Tester.Assert_Container
        ("Project_Object_Paths",
         Obtained,
         Expected);

      Obtained.Clear;
      Expected.Clear;


      declare
         Slice_Set            : GNAT.String_Split.Slice_Set;
         Slice_Number         : GNAT.String_Split.Slice_Number
           := GNAT.String_Split.Slice_Number'First;
         Project_Object_Paths : constant String
           := GNAT_GPR.Get_Project_Object_Paths (Project,
                                                 Recursive           => False,
                                                 Including_Libraries => False,
                                                 Exclude_Externally  => False,
                                                 Prepend             => "",
                                                 Postpend            => GNAT.OS_Lib.Path_Separator & "");
         use type GNAT.String_Split.Slice_Number;
      begin
         GNAT.String_Split.Create
           (Slice_Set,
            Project_Object_Paths,
            GNAT.OS_Lib.Path_Separator & "");
         Slice_Number := GNAT.String_Split.Slice_Count (Slice_Set);
         if Slice_Number >= GNAT.String_Split.Slice_Number'First + 1 then
            for Index in GNAT.String_Split.Slice_Number'First + 1 .. Slice_Number - 1 loop
               Obtained.Append (TS (GNAT.String_Split.Slice
                 (Slice_Set, Index)));
            end loop;
         end if;
      end;
      Expected.Append (TS ("objects"));

      Tester.Assert_Container
        ("Project_Object_Paths",
         Obtained,
         Expected);
   end Test_Lib_GPR_Analysis;


   procedure Test_GPR_Zoombies_Analysis
     (T : in out Aunit.Test_Cases.Test_Case'Class)
   is
      pragma Unreferenced (T);

      procedure Delete_File (Name : String)
      is
      begin
         if Ada.Directories.Exists (GNAT.OS_Lib.Normalize_Pathname (Name)) then
            Ada.Directories.Delete_File (GNAT.OS_Lib.Normalize_Pathname (Name));
         end if;
      end Delete_File;

      procedure Create_File (Name : String)
      is
         H : Ada.Text_IO.File_Type;
      begin
         Ada.Text_IO.Create (File => H,
                             Mode => Ada.Text_IO.Out_File,
                             Name => GNAT.OS_Lib.Normalize_Pathname (Name));
         Ada.Text_IO.Close (H);
      end Create_File;
      Project_Name : constant String := "../basic_gpr/basicgpr.gpr";
   begin
      Container.Clear;

      GNAT_GPR.Finalize;
      GNAT_GPR.Initialize;
      ---------------
      -- Project Zoombies
      ---------------
      Delete_File ("../basic_gpr/objects/zoombie.o");
      Delete_File ("../basic_gpr/objects/zoombie.ali");

      GNAT_GPR.Set_Project_Object_File_Zoombies (Project             => Project_Name,
                                                 Container           => Container,
                                                 Transitive_Analysis => True,
                                                 Exclude_Externally  => True);

      Tester.Assert_Container
        ("Project_Zoombies 1",
         Container,
         (others => GNAT_GPR.K_Null_Unit_Information));

      Create_File ("../basic_gpr/objects/zoombie.o");
      Create_File ("../basic_gpr/objects/zoombie.ali");

      GNAT_GPR.Set_Project_Object_File_Zoombies (Project             => Project_Name,
                                                 Container           => Container,
                                                 Transitive_Analysis => True,
                                                 Exclude_Externally  => True);

      GNAT_GPR.Display_Project_Object_File_Zoombies (Project_Name,
                                                     True,
                                                     True);

      Tester.Assert_Container
        ("Project_Zoombies 2",
         Container,
         ((TS (""),
          TS ("zoombie.o")),
         (TS (""),
          TS ("zoombie.ali")),
          others => GNAT_GPR.K_Null_Unit_Information));

      Delete_File ("../basic_gpr/objects/zoombie.o");
      Delete_File ("../basic_gpr/objects/zoombie.ali");

      -------


      Create_File ("../basic_gpr/sources/gnat_gpr-unused.ads");
      Create_File ("../basic_gpr/sources/gnat_gpr-unused-dummy.ads");
      Create_File ("../basic_gpr/sources/gnat_gpr-unused-dummy.adb");

      Container.Clear;
      GNAT_GPR.Set_Project_Source_File_Zoombies (Project               => Project_Name,
                                                 Container             => Container,
                                                 Transitive_Analysis   => True,
                                                 Exclude_Externally    => True,
                                                 Exclude_Library_Based => False);

      Delete_File ("../basic_gpr/sources/gnat_gpr-unused.ads");
      Delete_File ("../basic_gpr/sources/gnat_gpr-unused-dummy.ads");
      Delete_File ("../basic_gpr/sources/gnat_gpr-unused-dummy.adb");

      Tester.Assert_Container
        ("Project Source Zoombies",
         Container,
         ((TS (""),
          TS ("gnat_gpr-unused.ads")),
          (TS (""),
           TS ("gnat_gpr-unused-dummy.ads")),
          (TS (""),
           TS ("gnat_gpr-unused-dummy.adb")),
          others => GNAT_GPR.K_Null_Unit_Information));


   end Test_GPR_Zoombies_Analysis;



   -- Register test routines to call:
   procedure Register_Tests (T: in out Test_Case) is
   begin
      -- Repeat for each test routine:
      AUnit.Test_Cases.Registration.Register_Routine (T, Test_Lib_GPR_Analysis'Access, " Library");
      AUnit.Test_Cases.Registration.Register_Routine (T, Test_GPR_Analysis'Access, " Main");
      AUnit.Test_Cases.Registration.Register_Routine (T, Test_GPR_Zoombies_Analysis'Access, " Zoombies");
   end Register_Tests;


   function Name (T: Test_Case) return AUnit.Message_String is
      pragma Unreferenced (T);
   begin
      return new String'("GPR analyzis");
   end Name;

end Test_Case_GPR_Analysis;
