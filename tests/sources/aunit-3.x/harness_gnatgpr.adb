--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2017, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

-- Ada
with Ada.Command_Line,
     Ada.Directories;

-- GNAT
with GNAT.OS_Lib;

--AUnit
with AUnit.Test_Suites,
     AUnit.Extras.Run,
     AUnit.Extras.Reporters.Consoles;
--       AUnit.Run, -- not present in Debian libaunit1-dev 1.03-6
--       AUnit.Reporter.Text;  -- not present in Debian libaunit1-dev 1.03-6

--  List of test cases to run:
with GNAT_GPR;
with Test_Case_GPR_Analysis;
with Test_Case_Interfaces;

procedure Harness_GNATGPR is

   -------------
   -- Executable_Directory --
   -------------

   function Executable_Directory (Status  :    out Boolean;
                                  Command : in     String := Ada.Command_Line.Command_Name) return String
   is
      Executable : GNAT.OS_Lib.String_Access;
   begin
      Executable := GNAT.OS_Lib.Locate_Exec_On_Path (Command);
      declare
         Containing : constant String := Ada.Directories.Containing_Directory (Executable.all);
      begin
         if Ada.Directories.Exists (Containing) then
            Status := True;
            GNAT.OS_Lib.Free (Executable);
            return Containing;
         else
            Status := False;
            return "";
         end if;
      end;
   exception
      when others =>
         Status := False;
         return "";
   end Executable_Directory;

   Result            : aliased constant AUnit.Test_Suites.Access_Test_Suite
     := new AUnit.Test_Suites.Test_Suite;
   Reporter : AUnit.Extras.Reporters.Consoles.Console_Reporter;

   Test_GPR_Analysis : aliased Test_Case_GPR_Analysis.Test_Case;
   Test_Interfaces   : aliased Test_Case_Interfaces.Test_Case;
   --Reporter          : AUnit.Reporter.Text.Text_Reporter;

   function Suite return AUnit.Test_Suites.Access_Test_Suite is
   begin
      --  You may add multiple tests or suites here:
      AUnit.Test_Suites.Add_Test (Result, Test_Interfaces'Access);
      AUnit.Test_Suites.Add_Test (Result, Test_GPR_Analysis'Access);
      return Result;
   end Suite;

   procedure Run is new AUnit.Extras.Run.Test_Runner (Suite => Suite);
   Status : Boolean;

begin

   Ada.Directories.Set_Directory (Executable_Directory (Status));
   Reporter.Set_Use_ANSI_Colors (True);

   GNAT_GPR.Initialize;
   Run  (Reporter => Reporter);
   GNAT_GPR.Finalize;
end Harness_GNATGPR;
