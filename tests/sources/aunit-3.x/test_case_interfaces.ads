--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2018, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

-- AUnit
with AUnit.Test_Cases;


package Test_Case_Interfaces is
   type Test_Case is new AUnit.Test_Cases.Test_Case
   with null record;

   -- Register routines to be run:
   procedure Register_Tests (T: in out Test_Case);

   -- Provide name identifying the test case:
   function Name (T: Test_Case) return AUnit.Message_String;


end Test_Case_Interfaces;
