--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2018, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
with GNAT_GPR;

package body Test_Case_Interfaces is

   procedure Test_Replace_Char (T : in out Aunit.Test_Cases.Test_Case'Class)
   is
      procedure Check (Payload   : String;
                       From      : Character;
                       To        : String;
                       Expected : String)
      is
         Obtained : constant String := GNAT_GPR.Replace (Char => From,
                                                         To   => To,
                                                         Item => Payload);
      begin
         T.Assert (Condition => Obtained = Expected,
                   Message   => "obtained: '" & Obtained & "' expected: '" & Expected & "'");
      end Check;
   begin
      Check (Payload   => "//1",
             From      => '/',
             To        => "---",
             Expected => "------1");

      Check (Payload   => "//",
             From      => '/',
             To        => "--",
             Expected => "----");

      Check (Payload   => "/1",
             From      => '/',
             To        => "",
             Expected => "1");

      Check (Payload   => "/11",
             From      => '1',
             To        => "",
             Expected => "/");

      Check (Payload   => "//11//22//33//44//55//66//77//88//99",
             From      => '/',
             To        => "",
             Expected => "112233445566778899");

      Check (Payload   => "//11//22//33//44//55//66//77//88//99",
             From      => '1',
             To        => "",
             Expected => "////22//33//44//55//66//77//88//99");

      Check (Payload   => "//11//22//33//44//55//66//77//88//99",
             From      => '9',
             To        => "",
             Expected => "//11//22//33//44//55//66//77//88//");
   end Test_Replace_Char;

   procedure Test_Replace_String (T : in out Aunit.Test_Cases.Test_Case'Class)
   is
      procedure Check (Payload   : String;
                       From      : String;
                       To        : String;
                       Expected : String)
      is
         Obtained : constant String := GNAT_GPR.Replace (From => From,
                                                         To   => To,
                                                         Item => Payload);
      begin
         T.Assert (Condition => Obtained = Expected,
                   Message   => "obtained: '" & Obtained & "' expected: '" & Expected & "'");
      end Check;
   begin
      Check (Payload   => "//1",
             From      => "//",
             To        => "---",
             Expected => "---1");

      Check (Payload   => "//",
             From      => "//",
             To        => "--",
             Expected => "--");

      Check (Payload   => "/1",
             From      => "/",
             To        => "",
             Expected => "1");

      Check (Payload   => "/11",
             From      => "11",
             To        => "",
             Expected => "/");

      Check (Payload   => "//11//22//33//44//55//66//77//88//99",
             From      => "//",
             To        => "",
             Expected => "112233445566778899");

      Check (Payload   => "//11//22//33//44//55//66//77//88//99",
             From      => "11",
             To        => "",
             Expected => "////22//33//44//55//66//77//88//99");

      Check (Payload   => "//11//22//33//44//55//66//77//88//99",
             From      => "99",
             To        => "",
             Expected => "//11//22//33//44//55//66//77//88//");
   end Test_Replace_String;

   -- Register test routines to call:
   procedure Register_Tests (T: in out Test_Case) is
   begin
      -- Repeat for each test routine:
      AUnit.Test_Cases.Registration.Register_Routine (T, Test_Replace_Char'Access, " Replace Char");
      AUnit.Test_Cases.Registration.Register_Routine (T, Test_Replace_String'Access, " Replace String");
   end Register_Tests;

   function Name (T: Test_Case) return AUnit.Message_String is
      pragma Unreferenced (T);
   begin
      return new String'("GNAT_GPR interfaces");
   end Name;

end Test_Case_Interfaces;
