--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2018, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

--Ada
with --  Ada.Text_IO,
Ada.Containers,
     Ada.Directories;

use Ada.Directories;

-- AUnit
with AUnit.Assertions;

package body Tester is


   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------
   procedure To_Simple_Name (Container : in out GNAT_GPR.Containers.Map)
   is
      Copy : GNAT_GPR.Containers.Map;
      Unit : GNAT_GPR.T_Unit_Information;
   begin

      for Element of Container loop
         if Element.Parent_GPR_Name /= Null_Unbounded_String then
            Unit.Parent_GPR_Name := To_Unbounded_String (Ada.Directories.Simple_Name (To_String (Element.Parent_GPR_Name)));
         end if;
         Unit.Unit_File_Name := To_Unbounded_String (Ada.Directories.Simple_Name (To_String (Element.Unit_File_Name)));
         Copy.Include (Simple_Name (To_String (Unit.Unit_File_Name)),
                       Unit);
      end loop;
      Container.Clear;
      for Element of Copy loop
         Container.Include (Simple_Name (To_String (Element.Unit_File_Name)),
                            Element);
      end loop;

   end To_Simple_Name;

   -- --------------------------------------------------------------------------
   procedure Assert_Container (Process                         : in     String;
                               Obtained_Container              : in out GNAT_GPR.Containers.Map;
                               Expected_Unit_Information_Array : in     T_Unit_Information_Array)
   is
      use type GNAT_GPR.T_Unit_Information;
      use type Ada.Containers.Count_Type;
      use type GNAT_GPR.Containers.Cursor;
      Expected_Length : Ada.Containers.Count_Type := Ada.Containers.Count_Type'First;
      Cursor          : GNAT_GPR.Containers.Cursor;

      function Image_Obtained return String
      is
         R : Ada.Strings.Unbounded.Unbounded_String;
      begin
         for S of Obtained_Container loop
            Ada.Strings.Unbounded.Append (R, S.Unit_File_Name & " ");
         end loop;
         return To_String (R);
      end Image_Obtained;
   begin
      -- test apply only on the file simple name
      To_Simple_Name (Obtained_Container);

      for Index in Expected_Unit_Information_Array'Range loop
         if Expected_Unit_Information_Array (Index)
           = GNAT_GPR.K_Null_Unit_Information then
            exit;
         else
            Expected_Length := Ada.Containers.Count_Type'Succ (Expected_Length);

            Cursor          := Obtained_Container.Find (Simple_Name (To_String (Expected_Unit_Information_Array (Index).Unit_File_Name)));

            AUnit.Assertions.Assert (Cursor /= GNAT_GPR.Containers.No_Element,
                                     "Testing " & Process & "; " &
                                       To_String (Expected_Unit_Information_Array (Index).Unit_File_Name) & " " &
                                       To_String (Expected_Unit_Information_Array (Index).Parent_GPR_Name) &
                                       " not found in Obtained_Container");
         end if;
      end loop;

      -- compare Length
      AUnit.Assertions.Assert
        (Expected_Length = Obtained_Container.Length,
         Process &
         " Expected_Length (" & Ada.Containers.Count_Type'Image (Expected_Length) &
           	") /=  Obtained_Container.Length (" &
           Ada.Containers.Count_Type'Image (Obtained_Container.Length) & ")" & Image_Obtained);
   end Assert_Container;

   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------
   procedure To_Simple_Name (Container : in out String_Containers.Vector)
   is
      Copy : String_Containers.Vector;
   begin

      for Element of Container loop
         Copy.Append (To_Unbounded_String (Ada.Directories.Simple_Name (Ada.Directories.Containing_Directory (To_String (Element)))));
      end loop;
      Container := Copy;
   end To_Simple_Name;

   -- --------------------------------------------------------------------------
   procedure Assert_Container (Name               : in     String;
                               Obtained_Container : in out String_Containers.Vector;
                               Expected_Container : in     String_Containers.Vector)
   is
      use String_Containers;
      use type Ada.Containers.Count_Type;
      procedure Process (Cursor : in String_Containers.Cursor) is
         Expected : constant Unbounded_String := String_Containers.Element (Cursor);
      begin
         if Obtained_Container.Find (Expected) =  String_Containers.No_Element then


            AUnit.Assertions.Assert
              (False,
               "(EE) " & Name & " Missing " & To_String (Expected));

         end if;
      end Process;

   begin
      To_Simple_Name (Obtained_Container);

      Expected_Container.Iterate (Process'Access);
      AUnit.Assertions.Assert
        (Expected_Container.Length = Obtained_Container.Length,
         "Expected length /= Obtained length");

   end;

   -- --------------------------------------------------------------------------
   function Image (Units     : T_Unit_Information_Array) return String
   is
      Result : Unbounded_String;
   begin
      for Unit of Units loop
         Append (Result, Unit.Unit_File_Name & ASCII.LF);
      end loop;
      return To_String (Result);
   end Image;

   -- --------------------------------------------------------------------------
   function Image (Container : GNAT_GPR.Containers.Map) return String
   is
      Result : Unbounded_String;
   begin
      for Element of Container loop
         Append (Result, Element.Unit_File_Name & ASCII.LF);
      end loop;
      return To_String (Result);
   end Image;

   -- --------------------------------------------------------------------------
   function Image (Container : String_Containers.Vector) return String
   is
      Result : Unbounded_String;
   begin
      for Element of Container loop
         Append (Result, Element & ASCII.LF);
      end loop;
      return To_String (Result);
   end Image;
end Tester;
