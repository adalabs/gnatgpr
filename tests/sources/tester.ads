--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2017, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------

-- Ada
with Ada.Containers.Vectors,
     Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

-- GNATGPR
with GNAT_GPR;

package Tester is

   type T_Unit_Information_Array is array (1 .. 20) of GNAT_GPR.T_Unit_Information;


   -- --------------------------------------------------------------------------
   procedure Assert_Container (Process                         : in     String;
                               Obtained_Container              : in out GNAT_GPR.Containers.Map;
                               Expected_Unit_Information_Array : in     T_Unit_Information_Array);



  -- --------------------------------------------------------------------------
   package String_Containers is new Ada.Containers.Vectors
     (Index_Type   => Natural,
      Element_Type => Unbounded_String);


   -- --------------------------------------------------------------------------
   procedure Assert_Container
     (Name               : in     String;
      Obtained_Container : in out String_Containers.Vector;
      Expected_Container : in     String_Containers.Vector);

   function Image (Units     : T_Unit_Information_Array) return String;
   function Image (Container : GNAT_GPR.Containers.Map) return String;
   function Image (Container : String_Containers.Vector) return String;

end Tester;
