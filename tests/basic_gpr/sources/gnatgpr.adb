--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2011, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
-- Ada
with Ada.Text_IO,
     Ada.Strings.Unbounded;

use  Ada.Strings.Unbounded;

-- GNATGPR
with GNATGPR_Options,
     GNAT_GPR,
     Version_GNATGPR;

procedure GNATGPR is
begin
   GNATGPR_Options.Analyse_Options;

end GNATGPR;
