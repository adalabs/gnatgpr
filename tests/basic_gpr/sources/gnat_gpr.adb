--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2011, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
-- Ada
with Ada.Text_IO,
     Ada.Strings.Unbounded;

use  Ada.Strings.Unbounded;

-- GNAT
with GNAT.String_Split;

package body GNAT_GPR is
   -- --------------------------------------------------------------------------
   --
   -- --------------------------------------------------------------------------
   function Replace (Char : Character;
                     To   : String;
                     Item : String) return String is
      Source : constant Unbounded_String := To_Unbounded_String (Item);
      Result : Unbounded_String          := Null_Unbounded_String;
   begin
      for I in Item'Range loop
         if Item (I) = Char then
            Append (Result, To);
         else
            Append (Result,To_String (Unbounded_Slice
              (Source => Source,
               Low    => I,
               High   => I)));
         end if;
      end loop;
      --Ada.Text_IO.Put_Line (To_String (Result));
      return To_String (Result);
   end;

end GNAT_GPR;
