--                              GNATGPR                                     --
--                                                                          --
--                     Copyright (C) 2007-2011, AdaLabs Ltd                 --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--  You should have received a copy of the GNU General Public License       --
--  along with this program;                                                --
--  see the file COPYING3. If not, see <http://www.gnu.org/licenses/>.      --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
------------------------------------------------------------------------------
-- Ada
with Ada.Strings.Unbounded;

use Ada.Strings.Unbounded;

package GNATGPR_Options is

   Options_Error : exception;

   type T_Action_Kinds is (Help,
                           List_Sources,
                           List_Mains,
                           List_Projects,
                           List_Object_Paths,
                           Version);
   type T_Action_Value is array (T_Action_Kinds'Range) of
     Unbounded_String;

   Action       : T_Action_Kinds := Help;
   Action_Value : T_Action_Value := (others => Null_Unbounded_String);
   procedure Analyse_Options;
   -- Analyses and sets program options

   procedure Help_Options;
   -- Help on command line options
   procedure User_Message (Message : String);
end GNATGPR_Options;
